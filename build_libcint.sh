#!/bin/bash
#-------------------------------------------------------------#
export CurrDir=$PWD
export DepsDir=$CurrDir/hyperion/dependencies
export LibcintDir=$DepsDir/libcint
#-------------------------------------------------------------#
if ! command -v clisp &> /dev/null
then
  cp $DepsDir/patch_hess.patch $LibcintDir
  cd $LibcintDir
  patch -p0 -i patch_hess.patch
else
  cp $DepsDir/patch_autointor.patch $LibcintDir
  cd $LibcintDir
  patch -p0 -i patch_autointor.patch
  cd $LibcintDir/scripts
  clisp auto_intor.cl
  mv *.c $LibcintDir/src/autocode
fi
#-------------------------------------------------------------#
mkdir $LibcintDir/build
cd $LibcintDir/build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$DepsDir ..
make install
#-------------------------------------------------------------#
cd $CurrDir
python configlib.py
#-------------------------------------------------------------#
exit
