#!/usr/bin/env python3
import numpy as np
import h5py
import argparse
import pandas as pd


def get_corresponding_states(csvfile):
    df = pd.read_csv(csvfile)
    corresponding_idx = {}
    ref_file = df.columns[0]
    source_files = df.columns[1:]
    for fname in source_files:
        df1 = df[[ref_file, fname]]
        stateidx = df1.to_numpy()
        corresponding_idx[fname] = stateidx[~np.isnan(stateidx).any(axis=1), :]
        # Make states 0-indexed
        corresponding_idx[fname] -= np.ones(corresponding_idx[fname].shape)
    return corresponding_idx


def get_nsf(sih5):
    with h5py.File(str(sih5), 'r') as f:
        nsf = int(f.attrs.get('NSTATE'))
    return nsf


def get_tdms(sih5):
    with h5py.File(str(sih5), 'r') as f:
        sf_1tdm = f.get('SFS_TRANSITION_DENSITIES')[()]
        sf_spin1tdm = f.get('SFS_TRANSITION_SPIN_DENSITIES')[()]
    nbas2 = sf_1tdm.shape[2]
    return sf_1tdm, sf_spin1tdm, nbas2


def build_tdms(ref_nsf, source_tdms, source_spintdms, idx_table, nbas2):
    ref_1tdm = np.zeros((ref_nsf, ref_nsf, nbas2))
    ref_spin1tdm = np.zeros((ref_nsf, ref_nsf, nbas2))
    source_files = source_tdms.keys()
    for i in range(ref_nsf):
        for j in range(i, ref_nsf):
            fname, idxi, idxj = None, None, None
            for f in source_files:
                if i in idx_table[f][:, 0] and j in idx_table[f][:, 0]:
                    fname = f
                    idxi = np.where(idx_table[f][:, 0] == i)
                    idxi = int(idx_table[f][idxi, 1])
                    idxj = np.where(idx_table[f][:, 0] == j)
                    idxj = int(idx_table[f][idxj, 1])
                    break
            if fname is not None:
                ref_1tdm[i, j] = source_tdms[fname][idxi, idxj]
                ref_spin1tdm[i, j] = source_spintdms[fname][idxi, idxj]
    return ref_1tdm, ref_spin1tdm


def write_tdms(ref_file, ref_1tdm, ref_spin1tdm):
    with h5py.File(ref_file, 'a') as f:
        f['SFS_TRANSITION_DENSITIES'][:] = ref_1tdm
        f['SFS_TRANSITION_SPIN_DENSITIES'][:] = ref_spin1tdm


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('h5files', nargs='+', help='HDF5 files from OpenMolcas; TDMs are added to the first file')
    parser.add_argument('--states', help='CSV file containing correspondence relations between SF states from \
                                         each RASSI file')
    args = parser.parse_args()

    ref_file = args.h5files[0]
    source_files = args.h5files[1:]
    corresponding_idx = get_corresponding_states(args.states)

    ref_nsf = get_nsf(ref_file)
    source_tdms = {}
    source_spintdms = {}
    nbas2 = None
    for f in source_files:
        source_tdms[f], source_spintdms[f], nbas2_f = get_tdms(f)
        if nbas2 is not None and nbas2 != nbas2_f:
            raise ValueError('Basis sizes do not match!')
        nbas2 = nbas2_f

    ref_1tdm, ref_spin1tdm = build_tdms(ref_nsf, source_tdms, source_spintdms, corresponding_idx, nbas2)
    write_tdms(ref_file, ref_1tdm, ref_spin1tdm)
