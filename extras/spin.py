#!/usr/bin/env python3
import numpy as np


def spincomps_ms(spinmults):
    '''Constructs matrix representation of Sx, Sy, Sz in a basis
    of S, mS eigenfunctions.
    Input:
        spinmults - list of spin multiplicities'''
    Sx = []
    Sy = []
    Sz = []
    for smult in spinmults:
        spin = round(float(smult - 1) / 2, 1)
        ms = np.arange(-spin, spin + 1, 1)
        sx = np.zeros((int(smult), int(smult)), dtype = complex)
        sy = np.zeros((int(smult), int(smult)), dtype = complex)
        sz = np.zeros((int(smult), int(smult)))
        for i in range(smult):
            sz[i, i] = ms[i]
            if i < smult - 1:
                sx[i, i + 1] = 0.5 * np.sqrt((spin + ms[i] + 1) * \
                        (spin - ms[i]))
                sy[i, i + 1] = 0.5j * np.sqrt((spin + ms[i] + 1) * \
                        (spin - ms[i]))
            if i:
                sx[i, i - 1] = 0.5 * np.sqrt((spin - ms[i] + 1) * \
                        (spin + ms[i]))
                sy[i, i - 1] = - 0.5j * np.sqrt((spin - ms[i] + 1) * \
                        (spin + ms[i]))
        Sx.append(sx)
        Sy.append(sy)
        Sz.append(sz)
    return (Sx, Sy, Sz)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('spinmults', nargs = '+',
            help = 'spin multiplicities')
    args = parser.parse_args()
    mults = [int(multstr) for multstr in args.spinmults]
    (Sx, Sy, Sz) = spincomps_ms(mults)
    print('Sx', Sx)
    print('Sy', Sy)
    print('Sz', Sz)

