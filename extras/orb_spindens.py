#!/usr/bin/env python3
import numpy as np
import argparse

from hyperion.electronic_structure.read_h5 import get_rasscf
from hyperion.tools.matrix_tools import matrix_to_heatmap

# Parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('molcash5', help='HDF5 file from OpenMolcas')
parser.add_argument('--thr', default=10 ** (-6), type=float,
                    help='threshold for spin density matrix elements')
parser.add_argument('--plot', default=False, action='store_true',
                    help='plot a heatmap of the spin density matrix')
parser.add_argument('--absplot', default=False, action='store_true',
                    help='take absolute value of spin DM elements before plotting heatmap')
args = parser.parse_args()

(spinmult, mo_vecs, rdms, spindms, tdms, spintdms, ninact, nact, nroots) = \
    get_rasscf(args.molcash5)

for i in range(nroots):
    spinocc = np.zeros(nact)
    choices = []
    for j in range(nact):
        spinocc[j] = spindms[i][j, j]
        if np.amax(np.abs(spindms[i][:, j])) > args.thr and \
                np.amax(np.abs(spindms[i][j, :])) > args.thr:
            choices.append(j + 1)
    print('* ROOT ' + str(i + 1))
    print('  Diagonal elements of spin density matrix:')
    print(spinocc)
    print('  Orbitals with spin DM elements > ' + str(args.thr) + ':')
    print(choices)
    if args.absplot:
        matrix_to_heatmap(spindms[i], filename='abs-spindm_' + str(i + 1), cmap='cool')
    elif args.plot:
        matrix_to_heatmap(spindms[i], filename='spindm_' + str(i + 1), signed=True, cmap='seismic')
