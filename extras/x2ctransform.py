#!/usr/bin/env python3
import numpy as np
from hyperion.tools.matrix_tools import matrix_from_file


def x2c_transform(matr, lhs = 'uu', rhs = 'uu', retu = False,
                uufile = 'X2C_Uuu', lufile = 'X2C_Ulu',
                uuarray = None, luarray = None):
    '''Applies the X2C decoupling matrix U to matrix matr.
    Input:
    matr (nd.array or list of lists): 2D array to be transformed;
        Note: matr should be in primitive basis
    rhs, lhs (str, optional): used to specify which 2x2 block of the 
        4-component U is applied on each side of matr.
        rhs and lhs can be uu (upper-upper) or ul (upper-lower).
        (the other 2 blocks require knowledge of the positronic solution
        coefficients, which are not available in Molcas)
    retu (bool, optional): if True, return dictionary containing U matrix blocks
    uufile, lufile (str, optional): name of files containing matrix 
        elements of UU/LU blocks of the X2C decoupling matrix
    uuarray, luarray (nd.array/list of lists, optional): UU and LU blocks of
        the X2C decoupling matrix'''
    # Clean up input
    matr = np.array(matr)
    if len(matr.shape) != 2:
        raise ValueError('matr needs to be 2-dimensional')
    lhs = str(lhs).strip()
    lhs = lhs.lower()
    if lhs not in ['uu', 'lu']:
        lhs = input('Please provide a valid string for lhs (uu or lu\n \
            where u = upper/large component and l = lower/small component) : ')
    rhs = str(rhs).strip()
    rhs = rhs.lower()
    if rhs not in ['uu', 'lu']:
        rhs = input('Please provide a valid string for rhs (uu or lu \n \
            where u = upper/large component and l = lower/small component): ')
    # Store the 2x2 blocks for the 4-component transformation matrix U in a dict
    x2c_u = {}
    # Read U_uu (upper-upper i.e. top-left block of transformation matrix)
    if uuarray is None:
        x2c_u['uu'] = matrix_from_file(str(uufile))
    else:
        x2c_u['uu'] = np.array(uuarray)
    if 'l' in rhs or 'l' in lhs:
        if luarray is None:
            x2c_u['lu'] = matrix_from_file(str(lufile))
        else:
            x2c_u['lu'] = np.array(luarray)
    # Return LHS\dagger \cdot matr \cdot RHS
    if retu:
        return (x2c_u[lhs].T @ matr @ x2c_u[rhs], x2c_u)
    else:
        return x2c_u[lhs].T @ matr @ x2c_u[rhs]


def x2c_kinetic(kin):
    (kinx2c, x2c_u) = x2c_transform(kin, rhs = 'lu', retu = True)
    kinx2c += x2c_transform(kin, lhs = 'lu', uuarray = x2c_u['uu'],
                        luarray = x2c_u['lu'])
    kinx2c -= x2c_transform(kin, lhs = 'lu', rhs = 'lu',
                        uuarray = x2c_u['uu'], luarray = x2c_u['lu'])
    return kinx2c


def x2c_pvp(pvp):
    clight_au = 2.99792458 * 10**2 / 2.18769126364
    inv4c2 = 1 / (4 * clight_au ** 2)
    return np.multiply(inv4c2, x2c_transform(pvp, lhs = 'lu', rhs = 'lu'))


if __name__ == "__main__":
    import argparse
    from hyperion.tools.matrix_tools import matrix_to_file, compare_matrices
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('matrfile', 
            help = 'name of file containing untransformed matrix elements')
    parser.add_argument('-ref', default = None,
            help = 'path to file containing reference matrix elements')
    parser.add_argument('-leftlu', action = 'store_true',
            help = 'if specified, lower-upper X2C block is applied on the LHS')
    parser.add_argument('-rightlu', action = 'store_true',
            help = 'if specified, lower-upper X2C block is applied on the RHS')
    parser.add_argument('-t', dest = 'transf_kinetic', action = 'store_true',
            help = 'special flag for X2C transformation of kinetic matrix')
    parser.add_argument('-w', dest = 'transf_pvp', action = 'store_true',
            help = 'special flag for X2C transformation of pVp (a.k.a W)  matrix')
    args = parser.parse_args()
    # Read untransformed matrix from file
    matr_raw = matrix_from_file(args.matrfile)
    # Apply X2C transformation to matr_raw; kinetic and pVp matrices are special cases
    if args.transf_kinetic:
        matr_x2c = x2c_kinetic(matr_raw)
    elif args.transf_pvp:
        matr_x2c = x2c_pvp(matr_raw)
    else:
        (lhs, rhs) = ('uu', 'uu')
        if args.leftlu:
            lhs = 'lu'
        if args.rightlu:
            rhs = 'lu'
        matr_x2c = x2c_transform(matr_raw, lhs = lhs, rhs = rhs)
    if args.ref is None:
        # Print transformed matrix to file if a reference is not provided
        matrix_to_file(matr_x2c, filename = 'matr_x2c')
    else:
        #matrix_to_file(matr_x2c, filename = 'matr_x2c')
        # Compare transformed matrix to reference
        compare_matrices(matrix1 = matr_x2c, file2 = str(args.ref))

