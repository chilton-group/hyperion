# Changelog

<!--next-version-placeholder-->

## v1.0.1 (2022-06-16)
### Fix
* Defaults for orbdec_analysis ([`16a5265`](https://gitlab.com/chilton-group/hyperion/-/commit/16a52659efd0fb660c6e20e9955bcc64ece0b815))
