# **HYPERION**

HYPERION is a computational tool for calculating relativistic magnetic resonance parameters from active space wavefunctions.
It is written to work with HDF5 files from OpenMolcas.

## Installation
First, clone the repository using

`git clone https://gitlab.com/chilton-group/hyperion.git`

**Configuring dependencies**

HYPERION uses the **libcint** library by Qiming Sun to compute integrals in the AO basis. 
For prerequisites, check the main libcint repository: https://github.com/sunqm/libcint

The libcint source code needs to be patched to add hyperfine coupling spatial integrals.
From the top-level `hyperion` directory run the following two commands:

`git submodule update --init`

`./build_libcint.sh`

The `build_libcint.sh` script creates a `config.ini` file inside the lower-lever `hyperion` directory 
containing the location of the libcint library file.

**Installation via pip**

HYPERION is best installed in editable mode via pip.
From the top-level `hyperion` directory, run

`pip install -e .`

or, if installing on the Computational Shared Facility (CSF),

`pip install -e --user .`

This will automatically install missing Python pre-requisites, including `numpy`, `pandas`, `matplotlib` and `opt_einsum`.

## Configuring OpenMolcas 

In order to compute relativistic X2C-decoupled properties, HYPERION needs two X2C transformation matrices.
These can be obtained by patching the OpenMolcas source code.
Patch files can be found in the `dependencies` subdirectory. 
Use `patchmolcas-x2c.patch` for OpenMolcas versions older than `v21.02` and 
`patchmolcas-x2c-30jun21.patch` for `v21.02` and newer.

Clone the OpenMolcas repository (https://gitlab.com/Molcas/OpenMolcas) and 
copy the appropriate patch file to the top level directory of the OpenMolcas repository.
From the OpenMolcas directory, run

`patch -p0 -i NAME-OF-PATCHFILE`

to apply the patch. Then, compile OpenMolcas normally.

**Remember to add the `RX2C` keyword in the `&SEWARD` section of an OpenMolcas calculation to use X2C decoupling.**
Three plain-text files, `X2C_Uuu`, `X2C_Ulu` and `X2C_X` will be created in the scratch directory 
at the end of the SEWARD run. Only the first two are required by HYPERION.

To compute non-relativistic (non-picture-change-corrected) properties, use the `--nopc` flag when calling HYPERION.

**NOTE: If the `--nopc` flag has not been specified and the `X2C_Uuu` and `X2C_Ulu` are not found in the 
current directory, HYPERION stops**

## Usage

Run `hyperion -h` to see all available options.