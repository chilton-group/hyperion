#!/usr/bin/env python3
import numpy as np
import re 
import hyperion.tools.constants as const

from hyperion.tools.output_util import write_tensordata, write_3by3_tensor, write_pnmrinfo

class ParaNMR:
    def __init__(self, wavefunction, h1Zeeman, args):
        np.set_printoptions(precision=4)
        # Only StateInteraction wavefunctions are supported by the pNMR module
        if type(wavefunction).__name__ != 'StateInteraction':
            raise Exception('Hyperion pNMR is only compatible with RASSI and MPSSI wavefunctions')

        # Set up calculation options
        if args.temp is not None:
            self.temp = args.temp
        else:
            self.temp = [300.0] # Default to 300 K

        if args is None or args.mult is None:
            if args.automult: self.mult = np.array([int(mult) for mult in args.automult]) 
            else: self.mult = wavefunction.sf_smult
        else:
            self.mult = np.array([int(mult) for mult in args.mult]) #Specifying --mult overides the degeneracy detection which is in hyperion.py (!!?)

        self.beta = [1/(t * 1.380649e-23) for t in self.temp]  # !! Add boltzmann to const...
        self.so_energies = (wavefunction.so_energies - wavefunction.so_energies[0]) * 4.35974472e-18 # Convert Ha to J, relative to the ground state

        # Canonical partition function Q = sum_n exp(-beta * E_n)
        self.partfunc = [sum(np.exp(-self.so_energies * beta)) for beta in self.beta]

        # Initialise attributes
        self.h1Zeeman = -h1Zeeman * const.mu_bohr
        self.shieldtens = None
        self.pnmrshift = None

        write_pnmrinfo(self.temp)

        if abs(self.mult[0] - self.partfunc[0]) > 1:
            print('WARNING: the ground state multiplicity, {}, differs significantly from Q at {} K, {:.2f}.'.format(self.mult[0], self.temp[0], self.partfunc[0]))

        

    def shieldtens_singlestate(self, h1HFC, nuc):
        self.find_nucleus(nuc)
        self.h1HFC = h1HFC * 1e6 * const.hplanck / (const.mu_nuc * const.nuc_g[self.isotope])
        root = 0
        self.shieldtens = np.zeros((3,3), dtype=complex)
        for x in range(len(self.beta)):
            for y in range(len(self.mult)):
                mult = int(self.mult[y])
                self.shieldtens = np.zeros((3,3), dtype=complex)
                for i in range(3):
                    for j in range(3):
                        self.shieldtens[i][j] = np.trace(np.matmul(self.h1Zeeman[i, root:root+mult, root:root+mult],self.h1HFC[j, root:root+mult, root:root+mult]))
                self.shieldtens *= ( self.beta[x] / mult )
                self.shieldtens_diag = np.linalg.eig(self.shieldtens)
                self.write_results(self.shieldtens, self.temp[x], nuc, mode='Single State')
                root += mult

    def shieldtens_canonical(self, h1HFC, nuc, h1Zeeman=None):
        if h1Zeeman is not None: h1Zeeman = -h1Zeeman * const.mu_bohr
        else: h1Zeeman = self.h1Zeeman
        self.find_nucleus(nuc)
        self.partfunc = [sum(np.exp(-self.so_energies * beta)) for beta in self.beta]
        self.h1HFC = h1HFC * 1e6 * const.hplanck / (const.mu_nuc * const.nuc_g[self.isotope])
        maxroot = sum([int(x) for x in self.mult])

        for x in range(len(self.beta)):
            rootn = 0
            self.shieldtens = np.zeros((3,3), dtype=complex)
            self.shieldtens_curie = np.zeros((3,3), dtype=complex)
            self.shieldtens_lr = np.zeros((3,3), dtype=complex)
            for mult in self.mult:
                mult = int(mult)
                for i in range(3):
                    for j in range(3):
                        self.shieldtens_curie[i][j] += self.beta[x] * np.exp(-self.beta[x] * self.so_energies[rootn]) * np.trace(np.matmul(h1Zeeman[i, rootn:rootn+mult, rootn:rootn+mult],self.h1HFC[j, rootn:rootn+mult, rootn:rootn+mult]))
                for rootm in range(maxroot):
                    if rootm < rootn or rootm >= rootn + mult: #i.e. if rootm is not degenerate with rootn
                        for p in range(mult):
                            rootp = rootn + p
                            for i in range(3):
                                for j in range(3):
                                    self.shieldtens_lr[i][j] += 2 * np.real(np.exp(-self.beta[x] * self.so_energies[rootp]) * h1Zeeman[i,rootp,rootm] * self.h1HFC[j,rootm,rootp]) / (self.so_energies[rootm] - self.so_energies[rootp])
                rootn += mult
            self.shieldtens_curie /=  self.partfunc[x]
            self.shieldtens_lr /= self.partfunc[x]  # !! Add boltzmann to const...
            self.shieldtens = np.real(self.shieldtens_curie + self.shieldtens_lr)
            shift=self.write_results(self.shieldtens,self.temp[x], nuc)
        return shift

    def write_results(self, shieldtens, temp, nuc, mode='canonical', mult_id=0, seff=0):
        print(' * Temperature = {} K\n'.format(temp))
        shieldtens_diag = np.linalg.eig(shieldtens)
        if mode == 'singlestate':
            print(' * Pseudospin multiplet {}, S = {:.1f}\n'.format(mult_id + 1, seff))

        write_tensordata(shieldtens, 1e6*shieldtens_diag[0], shieldtens_diag[1], header1=' Shielding tensor', header2=' Eigenvalues (ppm) and eigenvectors of the shielding tensor:')

        print(' {} chemical shift of atom {} (ppm): {:.3f}'.format(mode, nuc, -1e6*np.average(shieldtens_diag[0])))
        return 1e6*np.average(shieldtens_diag[0])



    def find_nucleus(self, nuc): 
        # Parse nuc, get atom label and isotope label
        m = re.match('([0-9]*)([a-zA-z]+)([0-9]*)', nuc)
        elem = m.group(2).capitalize()
        if len(m.group(1)) == 0:  # isotope mass not specified
            # Look for most abundant magnetic isotope
            isotopes = const.isotopes[elem]
            isomass = 0
            abund = 0
            for imass in isotopes:
                iso = str(imass) + elem
                if const.nuc_spin[iso] != 0 and const.nuc_abundance[iso] > abund:
                    isomass = imass
                    abund = const.nuc_abundance[iso]
            if isomass == 0:
                raise ValueError('No magnetic isotopes were found for ' + elem)
        else:
            isomass = int(m.group(1))

        if len(m.group(3)) == 0:  # atom ID not specified
            atidx = 1
        else:
            atidx = int(m.group(3))
        self.nuc_mass, self.nuc_element, self.nuc_atidx = isomass, elem, atidx
        self.isotope = str(self.nuc_mass) + self.nuc_element

        # Print nucleus information
        if const.nuc_spin[self.isotope] == int(const.nuc_spin[self.isotope]):
            print('** ATOM {}{}, isotope {}, I = {:.0f} **'.format(self.nuc_element, self.nuc_atidx, self.isotope,
                                                                   const.nuc_spin[self.isotope]))
        else:
            print('** ATOM {}{}, isotope {}, I = {:.0f}/2 **'.format(self.nuc_element, self.nuc_atidx, self.isotope,
                                                                     2 * const.nuc_spin[self.isotope]))
