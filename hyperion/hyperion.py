#!/usr/bin/env python3
import argparse


from hyperion.electronic_structure import read_h5
from hyperion.electronic_structure.fock_space import FockSpace
from hyperion.electronic_structure.state_interaction import StateInteraction
from hyperion.electronic_structure.uhf import UHF

from hyperion.libcint_interface import LibcintInterface

from hyperion.hyperfine_coupling import HyperfineCoupling
from hyperion.zeeman import Zeeman

from hyperion.pnmr import ParaNMR

from hyperion.tools import output_util, dataframe_util

import numpy as np # !! get rid of this when automult is moved


def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('h5file', help='HDF5 file from OpenMolcas')

    parser.add_argument('nuc', nargs='+',
                        help='Specifies which nuclei to compute HFCCs for.\n'
                             'Labels should include isotope mass and atom index e.g. 14N1.\n'
                             'If nuclear mass is not specified, the most abundant '
                             'magnetic isotope is picked. The atom index has to match the '
                             'xyz/OpenMolcas input; if not specified, it is assumed to be 1.'
                        )

    parser.add_argument('--mult', default=None, nargs='+',
                        help='(Pseudo)spin multiplicity(ies)'
                        )

    parser.add_argument('--zeta', default=None, nargs='+', type=float,
                        help='Zeta parameter for the Gaussian nucleus model.\n'
                             'For multiple nuclei, the order must match the nuc arguments.'
                        )

    parser.add_argument('--hfc', action='store_true', default=None,
                        help='Compute relativistic HFCCs.'
                        )

    parser.add_argument('--gtens', action='store_true', default=False,
                        help='Compute relativistic Zeeman g-tensor.'
                        )

    parser.add_argument('--nosoc', action='store_true', default=False,
                        help='Evaluate final matrix elements in spin-free+spin basis.'
                        )

    parser.add_argument('--nopc', action='store_true', default=False,
                        help='Do not apply picture-change transformation.'
                        )

    parser.add_argument('--orbdec', default=None, nargs='*', type=int,
                        help='Request HFC orbital decomposition analysis. Optionally followed by one or more '
                             'root indices (1-indexed). By default, only the ground state (root 1) is analysed. '
                             'This option is only available for RASSCF, DMRGSCF and UHF/UKS wavefunctions.'
                        )

    parser.add_argument('--noplot', action='store_true', default=False,
                        help='Do not create matplotlib plots, only write matrix elements to a text file.\n'
                             'With this keyword, orbital decomposition analysis can be requested without '
                             'calling matplotlib functions.'
                        )

    parser.add_argument('--orbdec_labelfmt', default=None,
                        choices={'c', 'n', 'l', 'cn', 'cl', 'nl', 'lm', 'cnl', 'clm', 'nlm', 'cnlm'},
                        help='AO label format, where c = atom, n = electronic shell, l = angular momentum '
                             'and m = angular momentum projection.\n MOs are labelled according to the '
                             'dominating AO contribution.'
                        )

    parser.add_argument('--orbdec_min', default=None, type=float,
                        help='Minimum threshold for showing elements in the HFC orbital decomposition diagram.\n'
                             'Only for RASSCF, DMRGSCF and UHF/UKS wavefunctions.'
                        )

    parser.add_argument('--orbdec_nlevels', default=4, type=int,
                        help='Number of levels (orders of magnitude) to plot on the HFC orbital decomposition '
                             'diagram.'
                        )

    parser.add_argument('--guess', default=None,
                        help='rasscf.h5 file containing guess MOs for orbital decomposition analysis '
                             'using the transformed UHF/UKS spin density.'
                        )

    parser.add_argument('--relsign', action='store_true', default=False,
                        help='For the arbitrary pseudospin parametrisation, infer relative signs of '
                             'HFCCs and g-values with respect to their spin-dependent parts (FC+SD/spin-Zeeman).'
                        )

    parser.add_argument('--export', nargs='?', const='hyperion_export.csv', default=None,
                        help='Export all parameters computed during the current Hyperion run to a .csv file.\n'
                             'Can optionally be followed by a filename, with or without the .csv extension.\n'
                             'If a filename is not provided, data will be exported to hyperion_export.csv'
                        )

    parser.add_argument('--pnmr', action='store_true', default=False,
                        help='Calculate the shielding tensor. Includes paramagnetic contributions, excludes\n'
                            'diamagnetic contributions. Currently works on RASSI and MPSI wavefunctions only.'
                        )

    parser.add_argument('--temp', type=float, default=[300], nargs='+',
                        help='Temperatures at which shielding tensor is calculated, \n'
                            'default = 300 K.'
                        )
    
    args = parser.parse_args()
    
    return args


def init_electronic_structure(h5file, get_ao_overlap=False, mult=None):
    # Create wavefunction object from OpenMolcas HDF5 file and print general info
    molcas_module = read_h5.get_attribute(h5file, 'MOLCAS_MODULE')
    molcas_module = molcas_module.decode('UTF-8')
    estruct_data = read_h5.get_generalinfo(h5file, molcas_module)

    spinonly_mode = False

    if molcas_module == 'RASSCF':  # dmrgscf.h5 files have the molcas module attribute set to RASSCF
        if mult is None:
            # Spin-only (diagonal-only) mode is default for RASSCF/DMRGSCF, unless mult is specified
            spinonly_mode = True

        elec_struct = FockSpace(h5file, get_ao_overlap=get_ao_overlap, diagonal_only=spinonly_mode)
        output_util.write_molcasinfo(
            molcas_module, estruct_data['molcas_version'], elec_struct.aobasis.nbas,
            spinmult=elec_struct.spinmult, nroots=elec_struct.nroots,
            nactive=elec_struct.nact, ci_type=estruct_data['ci_type'], nelec=estruct_data['nactel'],
            nelec3=estruct_data['nelec3'], nhole1=estruct_data['nhole1']
        )
    elif molcas_module == 'RASSI' or molcas_module == 'MPSSI':
        elec_struct = StateInteraction(h5file, get_ao_overlap=get_ao_overlap)
        output_util.write_molcasinfo(
            molcas_module, estruct_data['molcas_version'], elec_struct.aobasis.nbas,
            spinmult=elec_struct.sf_smult, nroots=elec_struct.nsf
        )

    elif molcas_module == 'SCF':
        elec_struct = UHF(h5file, get_ao_overlap=get_ao_overlap)
        output_util.write_molcasinfo(
            molcas_module, estruct_data['molcas_version'], elec_struct.aobasis.nbas
        )
    else:
        raise Exception('Hyperion only works with RASSCF, DMRGSCF, RASSI, MPSSI and UHF/UKS data.')
    
    return elec_struct, spinonly_mode


def main():
    args = parse_arguments()
    print(type(args.pnmr), type(args.hfc), type(args.gtens))

    # Deal with orbdec keyword when no roots are specified
    if args.orbdec == []:
        args.orbdec = [1]

    # Print header and input information
    output_util.write_mainheader()
    output_util.write_inputinfo(args)

    # Only read AO overlap if doing guess mode orbital decomposition
    if args.guess is None and args.orbdec_labelfmt is None:
        get_ao_overlap = False
    else:
        get_ao_overlap = True

    # Default to HFCCs only, unless --gtens or --pnmr is set
    if args.hfc is None:
        if args.pnmr is False and args.gtens is False:
            args.hfc = True
        else:
            args.hfc = False


    elec_struct, spinonly_mode = init_electronic_structure(
        args.h5file,
        get_ao_overlap=get_ao_overlap,
        mult=args.mult
    )

    # Create Libcint interface and basis input
    cinter = LibcintInterface()
    cinter.create_input(elec_struct.aobasis)

    # if --mult isn't given and it's a RASSI/MPSI wavefunction, detect SO energy degeneracies; threshold is 1e-5 Ha
    if args.mult is None and type(elec_struct).__name__ == 'StateInteraction':
        args.automult = [1]
        for i in range(1,len(elec_struct.so_energies)):
            if abs(elec_struct.so_energies[i] - elec_struct.so_energies[i-1]) < 1e-5: 
                args.automult[-1] += 1
            else:
                args.automult.append(1)
        print('--mult not given, detected: \n {} \n \n    {} roots'.format(args.automult, sum(args.automult)))


    # Zeeman g-tensor section (if requested, or if pNMR requested)
    if args.gtens:
        zeeman = Zeeman(elec_struct, spinonly=spinonly_mode, args=args, cinter=cinter)
        zeeman.compute_gtens()
    elif args.pnmr:       
        zeeman = Zeeman(elec_struct, spinonly=spinonly_mode, args=args, cinter=cinter, verbose=False)
        zeeman.compute_h1()
    else:
        zeeman = None

    # Hyperfine coupling

    if args.hfc: 
        if args.pnmr: h1_hfc_list = []
        hfc = HyperfineCoupling(elec_struct, spinonly=spinonly_mode, args=args, cinter=cinter)
        for i in range(len(args.nuc)):
            if args.zeta is not None and len(args.zeta) > 1:
                hfc.compute_hfc(args.nuc[i].strip(), zeta=args.zeta[i])
            elif args.zeta is not None:  # same zeta for all args.nuc
                hfc.compute_hfc(args.nuc[i].strip(), zeta=args.zeta[0])
            else:  # No zeta
                hfc.compute_hfc(args.nuc[i].strip())
            if args.pnmr: h1_hfc_list.append(hfc.h1)
        
        if hfc.do_orbdec:
            hfc.orbdec.write_filelist()

        if args.export is not None:
            filename = args.export.strip()
            if filename[-4:] != '.csv':
                filename = filename.replace('.', '_')
                filename += '.csv'

            df = dataframe_util.make_export_dataframe(hfc, zeemanobj=zeeman)
            df.to_csv(filename, index=False)
            print(' All parameters have been exported to file {}'.format(filename))
    elif args.pnmr:
        h1_hfc_list = []
        hfc = HyperfineCoupling(elec_struct, spinonly=spinonly_mode, args=args, cinter=cinter, verbose=False)
        for i in range(len(args.nuc)):
            if args.zeta is not None and len(args.zeta) > 1:
                hfc.compute_h1(args.nuc[i].strip(), zeta=args.zeta[i])
            elif args.zeta is not None:  # same zeta for all args.nuc
                hfc.compute_h1(args.nuc[i].strip(), zeta=args.zeta[0])
            else:  # No zeta
                hfc.compute_h1(args.nuc[i].strip())
            h1_hfc_list.append(hfc.h1)

    # pNMR
    if args.pnmr:
        pnmr = ParaNMR(elec_struct, zeeman.h1, args)
        for i in range(len(args.nuc)):
            pnmr.shieldtens_canonical(h1_hfc_list[i], args.nuc[i])
