#!/usr/bin/env python3
import numpy as np
import opt_einsum as oe


import hyperion.analysis.tensor_util as tenstools
import hyperion.tools.matrix_tools as mtools
from hyperion.tools.output_util import write_tensordata, write_zeemaninfo, write_signtable
from hyperion.libcint_interface import LibcintInterface


class Zeeman:
    def __init__(self, wavefunction, spinonly=False, args=None, cinter=None, verbose=True):
        self.spinonly = spinonly
        self.rasscf_mode, self.uhf_mode = False, False
        # Bools for "special" modes; RASSI mode is considered default
        if type(wavefunction).__name__ == 'FockSpace':
            self.rasscf_mode = True  # used to determine what functions to call
        elif type(wavefunction).__name__ == 'UHF':
            self.uhf_mode = True

        # Store wavefunction object - instance of class FockSpace (RASSCF), StateInteraction (RASSI) or UHF (UHF/UKS)
        self.wfn = wavefunction

        # Write g-tensor (Zeeman) section header and description to output
        if verbose is True: write_zeemaninfo()

        # Set up calculation options
        if args is not None:
            self.nopc = args.nopc
        else:
            self.nopc = False

        if self.spinonly or self.uhf_mode or args is None:
            self.do_relative_signs = False
        else:
            self.do_relative_signs = args.relsign

        if self.rasscf_mode or self.uhf_mode:
            self.nosoc = True  # used to determine what matrix representation to use
        elif args is not None:
            self.nosoc = args.nosoc
        else:
            self.nosoc = False

        if args is not None and args.export is not None:
            self.do_export = True
            self.export_data = []
        else:
            self.do_export = False
            self.export_data = None

        # Set pseudospin multiplicities for which the g-tensor is computed
        if self.spinonly or self.uhf_mode:
            self.pseudos_mult = None
        elif args is None or args.mult is None:
            self.pseudos_mult = [self.wfn.sf_smult[0]]
            # this sets the pseudoS multiplicity = spin multiplicity
            # of the first spin-free state passed to RASSI/MPSSI
        else:
            self.pseudos_mult = [int(mlt) for mlt in args.mult]

        # Read X2C transformation matrix blocks
        self.x2c_uu, self.x2c_lu = None, None
        if not self.nopc:
            uufile = 'X2C_Uuu'
            lufile = 'X2C_Ulu'
            try:
                self.x2c_uu = mtools.matrix_from_file(uufile)  # UU block
            except FileNotFoundError:
                raise Exception(
                    'X2C transformation file {} was not found in the current directory'.format(uufile)
                )
            try:
                self.x2c_lu = mtools.matrix_from_file(lufile)  # LU block
            except FileNotFoundError:
                raise Exception(
                    'X2C transformation file {} was not found in the current directory'.format(lufile)
                )

        # Set attribute pointing to Libcint interface
        self.cinter = cinter

        # Initialise attributes holding spatial integrals and HFC operator matrices
        self.ints_contr, self.h1sz, self.h1oz, self.h1 = None, None, None, None

        # Initialise attributes holding tensor data (change with every multiplet)
        self.ggt, self.gtens, self.gvals, self.axes_g, self.eulang_g = None, None, None, None, None
        self.ggt_sz, self.gvals_sz, self.axes_sz, self.eulang_sz = None, None, None, None
        self.ggt_oz, self.gvals_oz, self.axes_oz, self.eulang_oz = None, None, None, None
        self.relsign_oz, self.relsign_g = None, None

    def spatial_integrals(self, gauge_origin=None):
        """Compute spatial part of the required integrals, picture-change transform if required, then
        convert to contracted basis. The final matrix is stored in self.ints_contr"""
        # Create Libcint interface if not already done
        if self.cinter is None:
            self.cinter = LibcintInterface()
            self.cinter.create_input(self.wfn.aobasis)
            # assume input was created if cinter exists

        if gauge_origin is not None:
            self.cinter.change_common_origin(np.array(gauge_origin))

        # Compute Zeeman integrals (0.5 * rc nabla) in normalised primitive basis
        ints_prim = 0.5 * self.cinter.int1e_sph('cint1e_irp_sph', 9)

        # X2C transform primitive array (if nopc is False)
        if not self.nopc:
            ints_prim = oe.contract('ia,kij->kaj', self.x2c_uu, ints_prim)
            ints_prim = oe.contract('kaj,jb->kab', ints_prim, self.x2c_lu)

        # Contract basis
        self.ints_contr = np.zeros((9, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        for k in range(9):
            self.ints_contr[k] = self.wfn.aobasis.prim_to_cgto(ints_prim[k])

    def hermitian_ints(self, zeeints):
        """Function that uses the matrix representation of Kronecker-product
        0.5*rc nabla (1st dimension of zeeints is the tensor component,
        2nd and 3rd dim = nbas) to construct Hermitian operator matrices
        (zee_sz used to compute the spin-Zeeman contribution to the g-tensor)
        and an anti-Hermitian combination of the cross-product integrals (zee_oz,
        used to evaluate the orbital-Zeeman contribution)."""
        zee_tens = zeeints + zeeints.transpose((0, 2, 1))
        zee_tens = zee_tens.reshape((3, 3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        # 1st index in reshaped array is vector field (rc) component
        # 2nd index is the component of nabla acting on an orbital

        zee_sz = np.zeros((3, 3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        zee_sz += zee_tens
        for k in range(3):
            zee_sz[k, k] -= zee_tens[0, 0] + zee_tens[1, 1] + zee_tens[2, 2]

        zee_oz = np.zeros((3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        zee_oz[0] = zeeints[5] - zeeints[7]
        zee_oz[1] = zeeints[6] - zeeints[2]
        zee_oz[2] = zeeints[1] - zeeints[3]
        zee_oz = 1j * (zee_oz.transpose((0, 2, 1)) - zee_oz)
        return zee_sz, zee_oz

    def h1_rasscf(self):
        """Determines Zeeman perturbation operators for a RASSCF wavefunction."""
        # Convert Zeeman integrals to MO basis
        zeeint_mo = np.zeros((9, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        for k in range(9):
            zeeint_mo[k] = self.wfn.ao2mo(self.ints_contr[k])
        # Hermitian operators are needed before contracting with 1-TDMs
        sz_mo, oz_mo = self.hermitian_ints(zeeint_mo)
        # add factor of 2 from converting Pauli matrices to spin operators
        sz_mo *= 2

        # Compute matrices in spin-free and spin-free+spin bases
        if self.spinonly:
            sz_sf = np.zeros((3, 3, 1, self.wfn.nroots))
            for k in range(3):
                for l in range(3):
                    sz_sf[k, l] = 0.5 * self.wfn.mo2ci(sz_mo[k, l], dm='spin', diagonal_only=True)
            self.h1sz = sz_sf.transpose((1, 0, 2, 3)).reshape((3, 3, self.wfn.nroots))
            self.h1 = self.h1sz
        else:
            # MO basis -> SF state (CI roots) basis: contract integrals with 1-TDMs
            sz_sf = np.zeros((3, 3, self.wfn.nroots, self.wfn.nroots))
            for k in range(3):
                for l in range(3):
                    sz_sf[k, l] = self.wfn.mo2ci(sz_mo[k, l], dm='we')

            oz_sf = np.zeros((3, self.wfn.nroots, self.wfn.nroots), dtype=complex)
            for k in range(3):
                oz_sf[k] = self.wfn.mo2ci(oz_mo[k])

            # SF -> SFS basis
            self.h1sz = np.zeros((3, self.wfn.nsfspin, self.wfn.nsfspin), dtype=complex)
            self.h1oz = np.zeros((3, self.wfn.nsfspin, self.wfn.nsfspin), dtype=complex)
            for k in range(3):
                self.h1oz[k] = self.wfn.ci2sfs(oz_sf[k])
                self.h1sz[k] = self.wfn.ci2sfs_spin(sz_sf[:, k])

            self.h1 = self.h1sz + self.h1oz

    def h1_rassi(self):
        """Determines Zeeman perturbation operators for a RASSI wavefunction."""
        # (anti-)Hermitian operators are needed before contracting with 1-TDMs
        sz_ao, oz_ao = self.hermitian_ints(self.ints_contr)
        # Add factor of 2 from converting Pauli matrices to spin operators
        sz_ao *= 2

        # AO basis -> SF state basis: contract integrals with 1-TDMs
        sz_sf = np.zeros((3, 3, self.wfn.nsf, self.wfn.nsf))
        for k in range(3):
            for l in range(3):
                sz_sf[k, l] = self.wfn.ao2sf(sz_ao[k, l], dm='we')

        oz_sf = np.zeros((3, self.wfn.nsf, self.wfn.nsf), dtype=complex)
        for k in range(3):
            oz_sf[k] = self.wfn.ao2sf(oz_ao[k])

        h1sz_sfs = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
        h1oz_sfs = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
        for k in range(3):
            h1oz_sfs[k] = self.wfn.sf2sfs(oz_sf[k])
            h1sz_sfs[k] = self.wfn.sf2sfs_spin(sz_sf[:, k])

        if self.nosoc:
            self.h1sz, self.h1oz = h1oz_sfs, h1oz_sfs
        else:
            # Convert perturbation operator h1_sfs to SO basis
            self.h1sz = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
            self.h1oz = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
            for k in range(3):
                self.h1sz[k] = self.wfn.sfspin2so(h1sz_sfs[k])
                self.h1oz[k] = self.wfn.sfspin2so(h1oz_sfs[k])

        self.h1 = self.h1sz + self.h1oz

    @staticmethod
    def diag_tensor(h1, spin):
        """Constructs and diagonalises the HFC tensor a
        Returns eigenvalues, eigenvectors and the symmetrised tensor"""
        gtens = h1 / spin
        if (np.abs(gtens) < 1e-16).all():
            # don't bother diagonalising tensors that are basically 0
            gvals, axes_g = np.zeros(3, dtype=float), np.eye(3, dtype=float)
        else:
            gvals, axes_g = np.linalg.eigh(gtens)
        return gvals, axes_g, gtens

    @staticmethod
    def diag_symmtensor(h1, spin):
        """Constructs and diagonalises the symmetrised g tensor G = g g^T
        Returns eigenvalues, eigenvectors and the symmetrised tensor"""
        ggt = np.real(oe.contract('kij,lji->kl', h1, h1))
        ggt *= float(3) / (spin * (spin + 1) * (2 * spin + 1))

        if (ggt < 1e-16).all():
            # don't bother diagonalising tensors that are basically 0
            gvals2, axes_g = np.zeros(3, dtype=float), np.eye(3, dtype=float)
        else:
            gvals2, axes_g = np.linalg.eigh(ggt)
            if not (gvals2 >= 0).all():
                print('Warning! Negative eigenvalues found for the symmetrised g tensor.')

        return gvals2, axes_g, ggt

    def write_results(self, mult_id, seff):
        if self.uhf_mode:
            print(' * Multiplet {}, S = {:.3f}\n'.format(mult_id + 1, seff))
        elif self.spinonly:
            print(' * Spin multiplet {}, S = {:.1f}\n'.format(mult_id + 1, seff))
        else:
            print(' * Pseudospin multiplet {}, S = {:.1f}\n'.format(mult_id + 1, seff))

        if seff == 0:
            parametrisation = None
            print('  No Zeeman splitting for singlet state.')
        elif self.spinonly or self.uhf_mode:
            parametrisation = 'spin-only'
            write_tensordata(self.gtens,
                             self.gvals,
                             self.axes_g,
                             angles=self.eulang_g,
                             header1='  Spin-Zeeman g-tensor:',
                             header2='  Spin-Zeeman g-values, main axes and Euler angles:',
                             evalname='g')
        else:
            parametrisation = 'pseudospin'
            # Write out data for spin-Zeeman, orbital-Zeeman and total tensors
            write_tensordata(self.ggt_sz,
                             self.gvals_sz,
                             self.axes_sz,
                             angles=self.eulang_sz,
                             header1='  Spin-Zeeman g-tensor:',
                             header2='  Spin-Zeeman g-values, main axes and Euler angles:',
                             evalname='g')

            write_tensordata(self.ggt_oz,
                             self.gvals_oz,
                             self.axes_oz,
                             angles=self.eulang_oz,
                             header1='  Orbital-Zeeman g-tensor:',
                             header2='  Orbital-Zeeman g-values, main axes and Euler angles:',
                             evalname='g')

            write_tensordata(self.ggt,
                             self.gvals,
                             self.axes_g,
                             angles=self.eulang_g,
                             header1='  Full symmetrised G-tensor:',
                             header2='  g-values, main axes and Euler angles of the full g-tensor:',
                             evalname='g')

            if self.do_relative_signs:
                write_signtable(self.relsign_oz,
                                self.relsign_g,
                                partnames=['Spin-Zeeman', 'Orbital-Zeeman', 'Total'],
                                evalname='g')

        print()
        
        if self.do_export and seff != 0:
            temp_data = [
                'Zeeman',
                mult_id + 1,
                seff,
                parametrisation,
                np.nan, np.nan, np.nan,  # NaNs for the nuclei columns, which are not filled for g-tensors
            ]
            if self.spinonly or self.uhf_mode:
                # Fill the PSO columns with NaNs
                temp_data += [
                    *self.gvals_sz,
                    *self.eulang_sz,
                    np.nan, np.nan, np.nan,
                    np.nan, np.nan, np.nan,
                    *self.gvals,
                    *self.eulang_g
                ]
            else:
                temp_data += [
                    *self.gvals_sz,
                    *self.eulang_sz,
                    *self.gvals_oz,
                    *self.eulang_oz,
                    *self.gvals,
                    *self.eulang_g
                ]
            if self.do_relative_signs:
                temp_data += [
                    *self.relsign_oz,
                    *self.relsign_g
                ]

            self.export_data.append(temp_data)

    def zeeman_uhf(self):
        sz_ao, oz_ao = self.hermitian_ints(self.ints_contr)
        # Add factor of 2 to spin-Zeeman part
        sz_ao *= 2

        self.h1sz = 0.5 * oe.contract('ij,abij->ba', self.wfn.spindm, sz_ao)
        # 1st index of h1sz couples with S, 2nd dimension couples with B
        self.gvals, self.axes_g, self.gtens = self.diag_tensor(self.h1sz, self.wfn.Sz)
        self.eulang_g = tenstools.rotation_to_euler_angles(self.axes_g)
        self.gvals_sz, self.axes_sz, self.eulang_sz = self.gvals, self.axes_g, self.eulang_g

        self.write_results(0, self.wfn.Sz)

    def zeeman_spinonly(self):
        """Only computes spin-Zeeman part - here for consistency, however should always give gvals = 2,
        so kind of useless"""
        self.h1_rasscf()

        for rt in range(self.wfn.nroots):
            self.gvals, self.axes_g, self.gtens = self.diag_tensor(self.h1sz[:, :, rt], self.wfn.S)
            self.eulang_g = tenstools.rotation_to_euler_angles(self.axes_g)
            self.gvals_sz, self.axes_sz, self.eulang_sz = self.gvals, self.axes_g, self.eulang_g
            self.write_results(rt, self.wfn.S)

    def zeeman_pseudospin(self):
        if self.rasscf_mode:
            self.h1_rasscf()
        else:
            self.h1_rassi()

        idx = 0
        for mult_id, mult in enumerate(self.pseudos_mult):
            s = round(float(mult - 1) / 2, 1)
            if s == 0:  # skip singlets - no Zeeman splitting
                self.write_results(mult_id, s)
                idx += mult
                continue

            islice = slice(idx, idx + mult)

            # Spin-dependent (spin-Zeeman) contribution
            self.gvals_sz, self.axes_sz, self.ggt_sz = self.diag_symmtensor(self.h1sz[:, islice, islice], s)
            self.gvals_sz = np.sqrt(np.abs(self.gvals_sz))

            # Spin-independent (orbital-Zeeman) contribution
            self.gvals_oz, self.axes_oz, self.ggt_oz = self.diag_symmtensor(self.h1oz[:, islice, islice], s)
            self.gvals_oz = np.sqrt(np.abs(self.gvals_oz))

            # Total g-tensor, eigenvalues and eigenvectors
            self.gvals, self.axes_g, self.ggt = self.diag_symmtensor(self.h1[:, islice, islice], s)
            self.gvals = np.sqrt(np.abs(self.gvals))

            # Reorder spin- and orbital-Zeeman g-vals and axes to maximise overlap with main axes of complete g-tensor
            reorder_sz = tenstools.reorder_by_overlap(self.axes_sz, self.axes_g)
            self.gvals_sz = self.gvals_sz[reorder_sz]
            self.axes_sz = self.axes_sz[:, reorder_sz]

            reorder_oz = tenstools.reorder_by_overlap(self.axes_oz, self.axes_g)
            self.gvals_oz = self.gvals_oz[reorder_oz]
            self.axes_oz = self.axes_oz[:, reorder_oz]
            
            # Compute Euler angles
            self.eulang_sz = tenstools.rotation_to_euler_angles(self.axes_sz)
            self.eulang_oz = tenstools.rotation_to_euler_angles(self.axes_oz)
            self.eulang_g = tenstools.rotation_to_euler_angles(self.axes_g)

            # Relative signs of orbital-Zeeman and total eigenvalues
            if self.do_relative_signs:
                self.relsign_oz, self.relsign_g = tenstools.relative_sign_by_magnitude([self.gvals_sz,
                                                                                        self.gvals_oz,
                                                                                        self.gvals])

            self.write_results(mult_id, s)

            # Increment idx to go to the next multiplet
            idx += mult

    def compute_gtens(self, gauge_origin=None):
        # Print special settings
        if gauge_origin is not None:
            print(' ...gauge origin is set to: ' + ' '.join(gauge_origin))

        # Compute and diagonalise g-tensor for specified multiplets
        self.spatial_integrals(gauge_origin=gauge_origin)

        if self.uhf_mode:
            self.zeeman_uhf()
        elif self.spinonly:
            self.zeeman_spinonly()
        else:
            self.zeeman_pseudospin()

    def compute_h1(self, gauge_origin=None):
        # Compute and diagonalise g-tensor for specified multiplets
        self.spatial_integrals(gauge_origin=gauge_origin)
        
        if self.rasscf_mode:
            self.h1_rasscf()
        else:
            self.h1_rassi()
