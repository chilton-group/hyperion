#!/usr/bin/env python3
import pandas as pd
from hyperion.analysis.tensor_util import euler_angles_to_rotation

export_column_dtypes = {
    'property': 'category',
    'multiplet_id': 'int',
    'effective_spin': 'float',
    'parametrisation': 'category',
    'isotope': 'category',
    'atom_id': 'category',
    'nuclear_spin': 'float',
    'spin_part_eigval1': 'float',
    'spin_part_eigval2': 'float',
    'spin_part_eigval3': 'float',
    'spin_part_euler_alpha': 'float',
    'spin_part_euler_beta': 'float',
    'spin_part_euler_gamma': 'float',
    'orbital_part_eigval1': 'float',
    'orbital_part_eigval2': 'float',
    'orbital_part_eigval3': 'float',
    'orbital_part_euler_alpha': 'float',
    'orbital_part_euler_beta': 'float',
    'orbital_part_euler_gamma': 'float',
    'total_eigval1': 'float',
    'total_eigval2': 'float',
    'total_eigval3': 'float',
    'total_euler_alpha': 'float',
    'total_euler_beta': 'float',
    'total_euler_gamma': 'float'
}

relsign_column_dtypes = {
    'orbital_part_relative_sign1': 'category',
    'orbital_part_relative_sign2': 'category',
    'orbital_part_relative_sign3': 'category',
    'total_relative_sign1': 'category',
    'total_relative_sign2': 'category',
    'total_relative_sign3': 'category'
}


def make_export_dataframe(hfcobj, zeemanobj=None):
    # Although nucleus data is not needed for g-tensors, having the same columns for HFC and Zeeman
    # is the only way to avoid pandas hogging memory and time when exporting
    export_columns = list(export_column_dtypes.keys())
    if hfcobj.do_relative_signs:
        export_columns += list(relsign_column_dtypes.keys())

    data_list = hfcobj.export_data
    if zeemanobj is not None:
        data_list += zeemanobj.export_data

    df = pd.DataFrame(data_list, columns=export_columns)
    df = df.astype({'property': 'category', 'parametrisation': 'category'}, copy=False)

    return df


def select_df_rows(df, criteria_dict):
    """
    Select rows in a dataframe based on criteria provided as a dictionary, where the key corresponds to the name of
    a column and the dictionary elements are lists of accepted values for a specific column.
    """
    for key in criteria_dict.keys():
        df = df[df[key].isin(criteria_dict[key])]

    df.reset_index()
    return df


def gen_gtens_axes(df):
    """
    Finds record of Zeeman parameters in Hyperion export csv, gets Euler angles, computes and returns
    numpy.ndarray of g tensor main axes as column vectors
    Args:
        df (pd.DataFrame) : dataframe with contents of Hyperion export csv file

    Returns:
        gaxes (np.ndarray)  : array of g tensor axes as column vectors
    """
    eulang = df.loc[
        df.property == 'Zeeman',
        ['total_euler_alpha', 'total_euler_beta', 'total_euler_gamma']
    ].to_numpy().flatten()

    gaxes = euler_angles_to_rotation(*eulang)

    return gaxes
