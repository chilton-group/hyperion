#!/usr/bin/env python3
import numpy as np
import ctypes
import argparse
import copy
import os
import configparser


from hyperion.electronic_structure.basis import Basis
from hyperion.tools.matrix_tools import compare_matrices, matrix_to_file


# Define pointers for libcint env, atm and bas arrays
# env global parameters
PTR_LIGHT_SPEED = 0
PTR_COMMON_ORIG = 1
PTR_RINV_ORIG   = 4
PTR_RINV_ZETA   = 7
PTR_ENV_START   = 20
# atm slots
CHARGE_OF       = 0
PTR_COORD       = 1
NUC_MOD_OF      = 2
PTR_ZETA        = 3
RAD_GRIDS       = 4
ANG_GRIDS       = 5
ATM_SLOTS       = 6
# bas slots
ATOM_OF         = 0
ANG_OF          = 1
NPRIM_OF        = 2
NCTR_OF         = 3
KAPPA_OF        = 4
PTR_EXP         = 5
PTR_COEFF       = 6
BAS_SLOTS       = 8
    

class LibcintInterface:
    """Class that establishes an interface to C-based analytical integral library Libcint.
    Initialising an instance of this class will load the library into the _cint attribute.
    Then, the create_input class method has to be called to set up atom coordinates and basis set information,
    which Libcint takes as input.
    This interface can compute integrals in the contracted basis (if create_input is called with contracted=True)
    or in the primitive basis (default).
    If integrals are computed in the primitive basis, subsequent transformation
    to the contracted basis can be handled by methods of the Basis class."""

    def __init__(self):
        """Load libcint into Python object _cint via numpy ctypeslib. Libcint location is read from config.ini"""
        config_obj = configparser.ConfigParser()
        config_obj.read(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini'))

        libpath = config_obj['libcint']['path']
        libname = config_obj['libcint']['name']

        self._cint = np.ctypeslib.load_library(libname, libpath)

    def create_input(self, basobj, contracted=False):
        """ Creates the arrays and variables to pass to libcint
        from a Basis-type object."""
        # Set Libcint-specific variables
        natm = basobj.at_charges.size
        if contracted:
            nbas = basobj.nbasr
        else:
            nbas = basobj.nprimr
        self.atm = np.zeros((natm, ATM_SLOTS), dtype=np.int32)
        self.bas = np.zeros((nbas, BAS_SLOTS), dtype=np.int32)
        self.env = np.zeros(10000)
        # Assign env global variables (i.e. positions among the first 20)
        self.env[PTR_LIGHT_SPEED] = 137.03599967994  # in a.u., from NIST website
        self.env[PTR_COMMON_ORIG: PTR_COMMON_ORIG + 3] = [0.0, 0.0, 0.0]  # can be changed
        off = PTR_ENV_START

        # Build atm array and add corresponding data to env
        for i in range(0, natm):
            self.atm[i, CHARGE_OF] = basobj.at_charges[i]
            self.atm[i, PTR_COORD] = off
            self.env[off:off+3] = basobj.at_coords[i, :]
            if i == 0:
                # Use coordinates of atom 0 as rinv origin (default, can be changed)
                self.env[PTR_RINV_ORIG:PTR_RINV_ORIG+3] = self.env[off:off+3]
                self.env[PTR_RINV_ZETA] = 0  # default zeta=0 i.e. point nucleus
            off += 3

        # Build bas array using primitive basis (if contracted is False) and contracted basis otherwise
        # Contraction will have to be done after applying the X2C transformation (if contracted is False)
        # But first, call GTO normalisation routine
        if contracted:
            basobj.normalise_cgto()
            contr_idx = 0
        else:
            basobj.normalise_gto()
        for i in range(0, nbas):
            if contracted:
                self.bas[i, ATOM_OF] = basobj.cgtos[i, 0]
                self.bas[i, ANG_OF] = basobj.cgtos[i, 1]
                self.bas[i, NPRIM_OF] = basobj.cgto_terms[i]
                self.bas[i, NCTR_OF] = 1
                self.bas[i, KAPPA_OF] = 0
                self.bas[i, PTR_EXP] = off
                self.bas[i, PTR_COEFF] = off + basobj.cgto_terms[i]
                for j in range(basobj.cgto_terms[i]):
                    # exponent of primitive j in expansion of CGTO i
                    self.env[off + j] = basobj.expcoef[contr_idx, 0]
                    # coeff of primitive j in expansion of CGTO i
                    self.env[off + basobj.cgto_terms[i] + j] = basobj.expcoef[contr_idx, 1] * basobj.cgto_norms[i]
                    contr_idx += 1
                off += 2 * basobj.cgto_terms[i]
            else:
                self.bas[i, ATOM_OF] = basobj.prims[i, 0]
                self.bas[i, ANG_OF] = basobj.prims[i, 1]
                self.bas[i, NPRIM_OF] = 1
                self.bas[i, NCTR_OF] = 1
                self.bas[i, KAPPA_OF] = 0
                self.bas[i, PTR_EXP] = off
                self.bas[i, PTR_COEFF] = off + 1
                self.env[off] = basobj.prims[i, 2]  # exponent of primitive i
                self.env[off + 1] = basobj.gto_norms[i]  # coeff of primitive i
                off += 2  # move env pointer by 2 positions since 2 values were added

        # Make dictionary of (center_id, angmom) pair occurrences from bas
        # nprim = number of GTOs/CGTOs including angular components (confusing name, but nbas was already taken)
        # These will help convert matrices from libcint order -> Molcas order
        if contracted:
            self.primshells = basobj.aoshells
            self.nprim = basobj.nbas
        else:
            self.primshells = basobj.primshells
            self.nprim = basobj.nprim

        # Convert variables to C-compatible formats
        self.natm = ctypes.c_int(natm) 
        self.nbas = ctypes.c_int(nbas) 
        self.c_atm = self.atm.ctypes.data_as(ctypes.c_void_p)  # void type pointer to atm
        self.c_bas = self.bas.ctypes.data_as(ctypes.c_void_p)  # void type pointer to bas
        self.c_env = self.env.ctypes.data_as(ctypes.c_void_p)  # void type pointer to env

    def change_common_origin(self, coords):
        self.env[PTR_COMMON_ORIG: PTR_COMMON_ORIG + 3] = coords

    def change_rinv_origin(self, atomid):
        coordptr = self.atm[atomid, PTR_COORD]
        self.env[PTR_RINV_ORIG : PTR_RINV_ORIG + 3] = self.env[coordptr : coordptr + 3]

    def set_rinv_zeta(self, zeta):
        self.env[PTR_RINV_ZETA] = zeta

    def make_cintopt(self, name):
        # initialise optimiser to NULL pointer
        self.cintopt = ctypes.POINTER(ctypes.c_void_p)()
        foptinit = getattr(self._cint, name + '_optimizer')
        foptinit(ctypes.byref(self.cintopt), self.c_atm, self.natm, self.c_bas, self.nbas, self.c_env)

    def int1e_shell(self, intor, dim, i, di, j, dj):
        """Calls Libcint to evaluate integrals over a single pair of shells.
        Returns 1D buf array containing integrals in Libcint order."""
        # In memory, elements of buf are stored as i1j1d1, i2j1d1...iNj1d1...iNjNd1, i1j1d2 ....
        # i.e. Fortran-contiguous
        buf = np.empty((di, dj, dim), dtype=np.double, order='F')
        shls = (ctypes.c_int * 2)(i, j)
        # The next line calls a C function - DO NOT REMOVE SEMICOLON!!!!!!
        intor(buf.ctypes.data_as(ctypes.c_void_p), shls, self.c_atm, self.natm, self.c_bas, self.nbas, self.c_env);
        # Return a C-contiguous array so everything is consistent in int1e_sph
        return buf.copy(order='C')

    def int2e_shell(self, intor, dim, i, di, j, dj, k, dk, l, dl):
        """Calls Libcint to evaluate integrals over a combination of 2 pairs of shells.
        Returns 1D buf array containing integrals in Libcint order."""
        # In memory, elements of buf are stored as i1j1k1l1d1, i2j1k1l1d1...i1j2k1l1d1...iNjNkNlNd1, i1j1k1l1d2 ....
        # i.e. Fortran-contiguous
        buf = np.empty((di, dj, dk, dl, dim), dtype=np.double, order='F')
        shls = (ctypes.c_int * 4)(i, j, k, l)
        # The next line calls a C function - DO NOT REMOVE SEMICOLON!!!!!!
        intor(buf.ctypes.data_as(ctypes.c_void_p), shls, self.c_atm, self.natm, self.c_bas, self.nbas, self.c_env, self.cintopt);
        # Return a C-contiguous array so everything is consistent in int2e_sph
        return buf.copy(order='C')

    def int1e_sph(self, name, dim):
        """ Generic function for evaluating a 1e integral matrix in a spherical GTO basis
            Matrix elements are rearranged to match Molcas order:
            atom centre > angmom > mL,
            where primtives w/ same center, angmom and mL  are one after the other.
            BASIS SIZE(S):
                nbas  = (primitive) basis size not accounting for angular components
                self.nprim = number of primitives, including angular components
            Returns: 3D array (dim, self.nprim, self.nprim) of matrix elements in Molcas order."""
        # Initialise 3D array that will hold all matrix elements over the primitive basis
        bigmatr = np.zeros((dim, self.nprim, self.nprim))
        # Cast libcint function (attribute of _cint object) to intor
        intor = getattr(self._cint, name)  
        intor.restype = ctypes.c_void_p
        jtot = 0  # column counters
        djtot = 0
        for jblock in sorted(self.primshells.keys()):  # loop over (center, angmom) pairs
            dj = 2 * jblock[1] + 1  # l multiplicity is constant within a block
            nbas_j = self.primshells[jblock][0]
            itot = 0  # row counters
            ditot = 0
            for iblock in sorted(self.primshells.keys()):
                di = 2 * iblock[1] + 1
                nbas_i = self.primshells[iblock][0]
                for j in range(nbas_j):
                    for i in range(nbas_i):
                        tempmatr = self.int1e_shell(intor, dim, itot + i, di, jtot + j, dj)
                        # Assign tempmatr elements to bigmatr in Molcas order
                        # swap 2nd and 3rd dimensions in tempmatr to match bigmatr
                        bigmatr[:, ditot + i: ditot + di * nbas_i: nbas_i,
                                djtot + j: djtot + dj * nbas_j: nbas_j] = \
                                copy.deepcopy(tempmatr.transpose(2, 0, 1))
                itot += nbas_i              
                ditot += di * nbas_i
            jtot += nbas_j
            djtot += dj * nbas_j
        return bigmatr

    def int2e_sph(self, name, dim):
        """ Generic function for evaluating a 2e integral matrix in a spherical GTO basis
            Matrix elements are rearranged to match Molcas order:
            atom centre > angmom > mL,
            where primtives w/ same center, angmom and mL  are one after the other.
            BASIS SIZE(S):
                nbas  = (primitive) basis size not accounting for angular components
                self.nprim = number of primitives, including angular components
            Returns: 5D array (dim, self.nprim, self.nprim, self.nprim, self.nprim) of matrix elements
            in Molcas order."""
        # For 2-electron integrals, an optimiser is needed. Initialise once for all shells.
        self.make_cintopt(name)
        # Initialise 5D array that will hold all matrix elements over the primitive basis
        bigmatr = np.zeros((dim, self.nprim, self.nprim, self.nprim, self.nprim))
        # Cast libcint function (attribute of _cint object) to intor
        intor = getattr(self._cint, name)
        intor.restype = ctypes.c_void_p
        ltot = 0
        dltot = 0
        for lblock in sorted(self.primshells.keys()):
            dl = 2 * lblock[1] + 1  # l multiplicity is constant within a block
            nbas_l = self.primshells[lblock][0]
            ktot = 0  # column counters
            dktot = 0
            for kblock in sorted(self.primshells.keys()):
                dk = 2 * kblock[1] + 1  # l multiplicity is constant within a block
                nbas_k = self.primshells[kblock][0]
                jtot = 0  # column counters
                djtot = 0
                for jblock in sorted(self.primshells.keys()):  # loop over (center, angmom) pairs
                    dj = 2 * jblock[1] + 1  # l multiplicity is constant within a block
                    nbas_j = self.primshells[jblock][0]
                    itot = 0  # row counters
                    ditot = 0
                    for iblock in sorted(self.primshells.keys()):
                        di = 2 * iblock[1] + 1
                        nbas_i = self.primshells[iblock][0]
                        for l in range(nbas_l):
                            for k in range(nbas_k):
                                for j in range(nbas_j):
                                    for i in range(nbas_i):
                                        tempmatr = self.int2e_shell(intor, dim, itot + i, di, jtot + j, dj,
                                                                    ktot + k, dk, ltot + l, dl)
                                        # Assign tempmatr elements to bigmatr in Molcas order
                                        # move 5th dimension from tempmatr to the start to to match bigmatr
                                        bigmatr[:, ditot + i: ditot + di * nbas_i: nbas_i,
                                                djtot + j: djtot + dj * nbas_j: nbas_j,
                                                dktot + k: dktot + dk * nbas_k: nbas_k,
                                                dltot + l: dltot + dl * nbas_l: nbas_l] = \
                                                copy.deepcopy(tempmatr.transpose(4, 0, 1, 2, 3))
                        itot += nbas_i
                        ditot += di * nbas_i
                    jtot += nbas_j
                    djtot += dj * nbas_j
                ktot += nbas_k
                dktot += dk * nbas_k
            ltot += nbas_l
            dltot += dl * nbas_l
        return bigmatr


def test_rinvorigin(cinter, atindices):
    tensorcomps = {0: 'XX', 1: 'XY', 2: 'XZ', 3: 'YX', 4: 'YY', 5: 'YZ',
                6: 'ZX', 7: 'ZY', 8: 'ZZ'}
    for atid in atindices:
        cinter.change_rinv_origin(atid)
        print(cinter.env[PTR_RINV_ORIG : PTR_RINV_ORIG + 3])
        v = cinter.int1e_sph('cint1e_hyp_sph', 9)
        for k in range(9):
            matrix_to_file(v[k], filename='hypint_' + tensorcomps[k] + '_at' + str(atid))


def testsuite1(cinter, overlap = True, kinetic = True, nuclear = True, pVp = True,
                normalise = False, asymmetry = False):
    """Simple test suite that computes overlap, kinetic, nuclear potential (V) and pVp integral matrices
    and benchmarks them against Molcas equivalents. Stats are printed to standard output.
    Input:
        overlap, kinetic, nuclear, pVp: bools used to specify which integrals are tested; All True by default.
            To disable testing for a particular integral type, set corresponding bool argument to False.
        normalise = whether to normalise the data about the matrix difference; currently not very good
        asymmetry = if True, benchmark upper and lower triangles of Libcint matrix separately and
                    also evaluate Libcint matrix - transpose(Libcint matrix)"""
    if overlap:
        print('=================== OVERLAP TEST ===========================================================')
        print('Evaluating the difference between Libcint (matrix1) and Molcas (matrix2) overlap matrices...')
        matr_lib = cinter.int1e_sph('cint1e_ovlp_sph', 1)
        compare_matrices(matrix1 = matr_lib[0], file2 = 'primitiveS', normalise = normalise, 
                asymmetry = asymmetry)
    if kinetic:
        print('================ KINETIC MATRIX TEST =======================================================')
        print('Evaluating the difference between Libcint (matrix1) and Molcas (matrix2) kinetic matrices...')
        matr_lib = cinter.int1e_sph('cint1e_kin_sph', 1)            
        compare_matrices(matrix1 = matr_lib[0], file2 = 'primitiveT', normalise = normalise, 
                asymmetry = asymmetry)
    if nuclear:
        print('================ NUCLEAR POTENTIAL TEST ====================================================')
        print('Evaluating the difference between Libcint (matrix1) and Molcas (matrix2) potential matrices.')
        matr_lib = cinter.int1e_sph('cint1e_nuc_sph', 1)
        compare_matrices(matrix1 = matr_lib[0], file2 = 'primitiveV', normalise = normalise, 
                asymmetry = asymmetry)
    if pVp:
        print('=================== pVp MATRIX TEST ========================================================')
        print('Evaluating the difference between Libcint (matrix1) and Molcas (matrix2) pVp matrices...')
        matr_lib = cinter.int1e_sph('cint1e_pnucp_sph', 1)
        compare_matrices(matrix1 = matr_lib[0], file2 = 'primitiveW', normalise = normalise, 
                asymmetry = asymmetry)


def hyptest(cinter, normalise = False, asymmetry = False):
    tensorcomps = {0: 'XX', 1: 'XY', 2: 'XZ', 3: 'YX', 4: 'YY', 5: 'YZ',
                6: 'ZX', 7: 'ZY', 8: 'ZZ'}
    # First test -<i|nabla-rinv|nabla j> matrix elements a.k.a hyp_pn
    matr = cinter.int1e_sph('cint1e_hyp_sph', 9)
    matr2_t1 = cinter.int1e_sph('cint1e_iprinvip_sph', 9)
    matr2_t2 = cinter.int1e_sph('cint1e_rinvipip_sph', 9)
    matr2 = matr2_t1 + matr2_t2
    matr2 = matr2_t1
    # Compare all 9 tensor components
    print('Testing identity -<i|nabla-rinv|nabla j> = <nabla i|rinv|nabla j> + <i|rinv|nabla nabla j>')
    print('NB: LHS is matrix1, RHS is matrix2')
    print(' ')
    for k in range(9):
        print('TENSOR COMPONENT ' + str(k) + ' (' + tensorcomps[k] + ')')
        compare_matrices(matrix1 = matr[k, :, :], matrix2 = matr2[k, :, :], 
                normalise = normalise, asymmetry = asymmetry)
    # Next, test -<nabla i|nabla-rinv|j> matrix elements a.k.a hypconj_pn
    matr = cinter.int1e_sph('cint1e_hypconj_sph', 9)
    # matr2_t1 is the same for hyp and hypconj, don't evaluate again
    matr2_t2 = cinter.int1e_sph('cint1e_ipiprinv_sph', 9)
    matr2 = matr2_t1 + matr2_t2
    matr2 = matr2_t2
    # Compare all 9 tensor components
    print('Testing identity -<nabla i|nabla-rinv|j> = <nabla i|rinv|nabla j> + <nabla nabla i|rinv|j>')
    print('NB: LHS is matrix1, RHS is matrix2')
    print(' ')
    for k in range(9):
        print('TENSOR COMPONENT ' + str(k) + ' (' + tensorcomps[k] + ')')
        compare_matrices(matrix1 = matr[k, :, :], matrix2 = matr2[k, :, :], 
                normalise = normalise, asymmetry = asymmetry)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('molcash5', help='name of hdf5 file from Molcas')
    parser.add_argument('-atoms', nargs='+', type=int,
            help='list of atom indices (0-based) to set as rinv origins')
    args = parser.parse_args()
    # Create Basis object
    basobj = Basis(str(args.molcash5))
    # Initialise Libcint interface object (loads libcint)
    cinter = LibcintInterface(libpath='dependencies/lib')
    # Create libcint input variables
    cinter.create_input(basobj, contracted=False)
    # Execute tests
    hypint = cinter.int1e_sph('cint1e_hyp_sph', 9)
    #hypint = cinter.int1e_sph('cint1e_iprinvip_sph', 9)
    #hypint += cinter.int1e_sph('cint1e_rinvipip_sph', 9)
    print('Primitive array')
    print(hypint)
    hyp_cgto = np.zeros((9, basobj.nbas, basobj.nbas))
    for k in range(9):
        hyp_cgto[k] = basobj.prim_to_cgto(hypint[k])
    print('Contracted array')
    print(hyp_cgto)

