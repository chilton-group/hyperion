#!/usr/bin/env python3
import numpy as np
import opt_einsum as oe

from hyperion.electronic_structure import read_h5


class UHF:
    def __init__(self, uhfh5, basis=None, get_ao_overlap=False):
        uhfh5 = str(uhfh5)

        # Create new Basis object if not provided
        if basis is None:
            from hyperion.electronic_structure.basis import Basis
            self.aobasis = Basis(uhfh5, get_overlap=get_ao_overlap)
        else:
            self.aobasis = basis

        h5data = read_h5.get_uhf(uhfh5)
        self.alpha_occ = h5data['alpha_occ']
        self.alpha_movecs = h5data['alpha_movecs']
        self.beta_occ = h5data['beta_occ']
        self.beta_movecs = h5data['beta_movecs']
        # Just like MO vecs in FockSpace, one row = one MO

        # Build spin-summed density matrix and spin density matrix in AO basis (MO -> AO)
        dm_alpha = np.zeros((self.aobasis.nbas, self.aobasis.nbas))
        np.fill_diagonal(dm_alpha, self.alpha_occ)
        dm_alpha = oe.contract('ai,ab,bj->ij', self.alpha_movecs, dm_alpha, self.alpha_movecs)
        # dm_alpha is now the 1-electron alpha density matrix in AO basis

        dm_beta = np.zeros((self.aobasis.nbas, self.aobasis.nbas))
        np.fill_diagonal(dm_beta, self.beta_occ)
        dm_beta = oe.contract('ai,ab,bj->ij', self.beta_movecs, dm_beta, self.beta_movecs)
        # dm_beta is now the 1-electron beta density matrix in AO basis

        self.rdm = dm_alpha + dm_beta  # 1-electron reduced spin-summed DM in AO basis
        self.spindm = dm_alpha - dm_beta  # 1-electron reduced spin DM in AO basis

        # Compute S_z expectation value
        self.Sz = 0.5 * np.trace(self.spindm)
