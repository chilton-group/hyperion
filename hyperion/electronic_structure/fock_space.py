#!/usr/bin/env python3
import numpy as np
import opt_einsum as oe

import hyperion.analysis.orbital_util as orb
from hyperion.electronic_structure import read_h5


class FockSpace:
    def __init__(self, orbh5, basis=None, get_ao_overlap=False, diagonal_only=False):
        orbh5 = str(orbh5)

        # Create new Basis object if not provided
        if basis is None:
            from hyperion.electronic_structure.basis import Basis
            self.aobasis = Basis(orbh5, get_overlap=get_ao_overlap)
        else:
            self.aobasis = basis

        # Read wavefunction info from HDF5 file
        h5data = read_h5.get_rasscf(orbh5, diagonal_only=diagonal_only)
        self.spinmult = h5data['spinmult']
        self.mo_types = h5data['mo_types']
        self.ninact = h5data['ninact']
        self.nact = h5data['nact']
        self.mo_vecs = h5data['mo_vecs']
        self.nroots = h5data['nroots']
        self.rdms = h5data['rdms']
        self.spindms = h5data['spindms']
        self.tdms = h5data['tdms']
        self.spintdms = h5data['spintdms']

        # Compute WE-reduced spin density matrices (independently computed CG coeffs, NOT Molcas method)
        self.S = round(float(self.spinmult - 1) / 2, 1)
        # Multiplicative factor to obtain WE-reduced matrices from spin density matrices
        if self.S == 0:
            self.we_factor = 1
        else:
            self.we_factor = 0.5 * np.sqrt((self.S + 1) * (2 * self.S + 1) / self.S)

        # Store number of spin-free + spin states (useful later)
        self.nsfspin = self.spinmult * self.nroots

    def get_ao_percentages(self):
        return orb.compute_mo_contributions(
            self.mo_vecs.T,
            self.aobasis.ao_ovlp
        )

    def get_aogroup_percentages(self, pattern='cnlm'):
        return orb.group_and_sort(
            self.get_ao_percentages(),
            self.aobasis.bas_ids,
            self.aobasis.at_lbls,
            pattern=pattern
        )

    def ao2mo(self, matr_ao):
        return self.mo_vecs @ matr_ao @ self.mo_vecs.T

    def mo2ci(self, matr_mo, dm='spin-summed', diagonal_only=False):
        """Contracts matrix in MO basis with (transition) density matrices (spin-summed, spin or
        Wigner-Eckart reduced)"""
        # Select relevant (transition) density matrices
        if dm == 'spin':
            rdms, tdms = self.spindms, self.spintdms
        elif dm == 'we':
            # Compute WE-reduced density matrices
            rdms, tdms = {}, {}
            for key in self.spindms.keys():
                rdms[key] = self.we_factor * self.spindms[key]
            if self.spintdms is None:
                tdms = None
            else:
                for key in self.spintdms.keys():
                    tdms[key] = self.we_factor * self.spintdms[key]
        else:
            rdms, tdms = self.rdms, self.tdms

        if diagonal_only:
            matr_ci = np.zeros((1, self.nroots), dtype=matr_mo.dtype)
        else:
            matr_ci = np.zeros((self.nroots, self.nroots), dtype=matr_mo.dtype)

        # Compute contribution to diagonal from inactive orbs
        inactive_sum = float(0)
        if dm == 'spin-summed':
            for i in range(self.ninact):
                inactive_sum += 2 * matr_mo[i, i]
        matr_active = matr_mo[self.ninact: self.ninact + self.nact, self.ninact: self.ninact + self.nact]

        # Compute diagonal elements of matr_ci
        for i in range(self.nroots):
            if diagonal_only:
                matr_ci[0, i] = inactive_sum + oe.contract('ij,ij->', rdms[i], matr_active)
            else:
                matr_ci[i, i] = inactive_sum + oe.contract('ij,ij->', rdms[i], matr_active)

        # Off-diagonal elements
        if (not diagonal_only) and self.nroots > 1:
            for (i, j) in tdms.keys():
                matr_ci[i, j] = oe.contract('ij,ij->', tdms[(i, j)], matr_active)
                # TDM of i->j is the Hermitian conjugate of j->i TDM
                # Deal with complex matrices separately
                if np.iscomplexobj(matr_ci):
                    matr_ci[j, i] = matr_ci[i, j].conj()
                else:
                    matr_ci[j, i] = matr_ci[i, j]

        return matr_ci

    def ci2sfs(self, matr_sf):
        """Computes Kronecker product of identity matrix in |ms> basis
        with a matrix in the spin-free a.k.a CI roots basis (matr_sf).
        The resulting matrix (matr_sfs) is in the spin-free+spin basis."""
        matr_sfs = np.kron(matr_sf, np.identity(self.spinmult))

        return matr_sfs

    def ci2sfs_spin(self, matr_sf):
        """Computes matrix representation of a mixed spin-spatial operator,
        using a RASSI-like formalism.
        Input:
            matr_sf - 3D numpy array corrsponding to the matrix representstion
                of a vector operator; matr_sf[0, :, :] is the x component,
                matr_sf[1, :, :] is y, matr_sf[2, :, :] is z;
                each component is a nsf x nsf matrix
        """
        matr_sfs = np.zeros((self.nsfspin, self.nsfspin), dtype=complex)

        idx1 = 0
        for sf1 in range(self.nsfspin):
            idx2 = idx1
            for sf2 in range(sf1, self.nroots):
                if self.S == 0:
                    matr_sfs[idx1, idx2] = matr_sf[2, sf1, sf2]
                else:
                    for i in range(self.spinmult):
                        ms = i - self.S
                        # ms1 = ms2
                        matr_sfs[idx1 + i, idx2 + i] = ms * matr_sf[2, sf1, sf2]
                        if sf1 != sf2:
                            matr_sfs[idx2 + i, idx1 + i] = np.conj(matr_sfs[idx1 + i, idx2 + i])
                        # ms1 + 1 = ms2
                        if ms + 1 <= self.S:
                            matr_sfs[idx1 + i, idx2 + i + 1] = np.sqrt((self.S - ms) * (self.S + ms + 1)) / 2 \
                                                             * (matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            matr_sfs[idx2 + i + 1, idx1 + i] = np.conj(matr_sfs[idx1 + i, idx2 + i + 1])
                        # ms1 - 1 = ms2
                        if ms - 1 >= - self.S:
                            matr_sfs[idx1 + i, idx2 + i - 1] = - np.sqrt((self.S + ms) * (self.S - ms + 1)) / 2 \
                                                             * (- matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            matr_sfs[idx2 + i - 1, idx1 + i] = np.conj(matr_sfs[idx1 + i, idx2 + i - 1])
                idx2 += self.spinmult
            idx1 += self.spinmult

        if self.S > 0:
            # Add common (positive) S-dependent factor
            matr_sfs *= (self.S * (self.S + 1) * (2 * self.S + 1)) ** (- 0.5)
