#!/usr/bin/env python3
import numpy as np
from math import factorial, sqrt, pi

from hyperion.electronic_structure import read_h5

shells_by_angmom = ['s', 'p', 'd', 'f', 'g', 'h', 'i', 'k', 'l']
cartcomps = {1: 'x', -1: 'y', 0: 'z'}  # Molcas order for p shell components


class Basis:
    """Class for CGTO basis sets.
    Attributes set on initialisation:
        natoms = number of atoms
        nprimr = no. of primitives, not accounting for angular components
        nprim = no. of primitives incl. angular components
        nbasr = no. of contracted functions, not including angular components
        nbas = no. of contracted functions incl. angular components
        !! Basis sets are assumed spherical !!
        at_charges = (natoms, ) ndarray of atomic charges as integers
        at_coords = (natoms, 3) ndarray of atomic coords in Bohr
        at_lbls = (natoms, ) ndarray of atom labels as strings (no padding!)
        expcoef = (ncontractions, 2) array of exponents and coefficients
        contr = (ncontractions, 4) array where columns are:
                atom ID, angmom, shell number, index of primitive
        prims = (nprimr, 3) array of atom ID, angmom, exponent
        cgtos = (nbasr, 3) array of atom ID, angmom, shell ID
        cgto_terms = (nbasr) array, number of primitives in the expansion of
                each CGTO
        primshells = dict where keys are (atom ID, angmom) pairs and
                elements are tuples of (no. of radial functions in shell,
                number of angular functions preceding current shell)
        aoshells = dict where keys are (atom ID, angmom) pairs and
                elements are tuples of (no. of radial functions in shell,
                number of angular functions preceding current shell)
    Normalisation routines for atomic basis sets:
        self.normalise_gto() normalises primitives -> norms stored in
            self.gto_norms
        self.normalise_cgto() normalises CGTO expansions of UN-NORMALISED
            primitives -> stored in self.cgto_norms
        self.normalise_prim2cgto() normalises CGTO expansions of NORMALISED
            primitives -> stored in self.prim2cgto_norms
    """

    def __init__(self, orbh5, get_overlap=False):
        # Read info about orbital basis from Molcas HDF5 file
        orbh5 = str(orbh5)

        h5data = read_h5.get_atomcentres(orbh5)
        self.natoms = h5data['natoms']
        self.at_charges = h5data['at_charges']
        self.at_coords = h5data['at_coords']
        self.at_lbls = h5data['at_lbls']

        h5data = read_h5.get_basis(orbh5, get_overlap=get_overlap)
        self.expcoef = h5data['expcoef']
        self.bas_ids = h5data['bas_ids']
        self.nbas = h5data['nbas']
        self.ao_ovlp = h5data['ao_ovlp']

        prim_ids = h5data['prim_ids']
        # Make center id 0-indexed instead of 1-indexed
        prim_ids[:, 0] -= np.repeat(1, prim_ids.shape[0])

        # Create array of (atom index, angmom, exponent))
        gtoinfo = np.concatenate((prim_ids[:, 0:2], self.expcoef[:, :1]), axis=1)
        gtoindex, gtoinv = np.unique(gtoinfo, return_index=True, return_inverse=True, axis=0)[1:3]

        # Create array of unique primitives, preserving the order in the h5 file from Molcas
        self.prims = np.array([gtoinfo[i, :] for i in sorted(gtoindex)])
        self.nprimr = self.prims.shape[0]

        # Create mapping from unique prim indices in np.unique output 
        # -> prim indices in molcas order
        idxinv = sorted(range(gtoindex.size), key=gtoindex.__getitem__)
        idxinv = sorted(range(len(idxinv)), key=idxinv.__getitem__)
        cgto2gto = np.zeros((prim_ids.shape[0], 1))
        cgto2gto[:, 0] = np.array([idxinv[i] for i in gtoinv])
        # Add column with primitive indices (in Molcas order) to prim_ids
        self.contr = np.concatenate((prim_ids, cgto2gto), axis=1)
        # columns of self.contr: center id, angmom, shell id, primitive index

        # Add primtive indices within shells to self.prims
        idx = 0
        pshl_ids = np.zeros((self.nprimr, 1))
        pshl_ids[0, 0] = 0
        for i in range(1, self.nprimr):
            if self.prims[i, 0] == self.prims[i - 1, 0] and \
                    self.prims[i, 1] == self.prims[i - 1, 1]:
                idx += 1
            else:
                idx = 0
            pshl_ids[i, 0] = idx
        self.prims = np.concatenate((self.prims, pshl_ids), axis=1)

        # Count the number of primitives (incl angular components)
        self.nprim = 0
        for i in range(self.prims.shape[0]):
            self.nprim += 2 * int(self.prims[i, 1]) + 1
        # Count the number of CGTOs not including angular components
        (self.cgtos, self.cgto_terms) = np.unique(self.contr[:, :3], return_counts=True, axis=0)
        self.nbasr = self.cgtos.shape[0]

        '''Set up arrays of indices to locate functions in shells
        A shell is defined by ps = (atom ID, angular momentum)
        ps pairs are used as dict keys
        primshells[ps][0] = no. of radial functions in the shell
        primshells[ps][1] = total no. of anuglar functions
            from previous shells'''
        self.primshells = {}
        for sh in [(int(self.prims[i, 0]), int(self.prims[i, 1])) for i in range(self.nprimr)]:
            self.primshells[sh] = self.primshells.get(sh, 0) + 1
        idx = 0
        for sh in sorted(self.primshells.keys()):
            shlcount = self.primshells[sh]
            self.primshells[sh] = (shlcount, idx)
            idx += shlcount * (2 * sh[1] + 1)
        # aoshells has the same structure as primshells
        self.aoshells = {}
        for sh in [(int(self.cgtos[i, 0]), int(self.cgtos[i, 1])) for i in range(self.nbasr)]:
            self.aoshells[sh] = self.aoshells.get(sh, 0) + 1
        idx = 0
        for sh in sorted(self.aoshells.keys()):
            shlcount = self.aoshells[sh]
            self.aoshells[sh] = (shlcount, idx)
            idx += shlcount * (2 * sh[1] + 1)

        # Create basis function labels
        self.bas_labels = self.label_cgtos()
        # Initialise attributes that will hold normalisation constants
        self.gto_norms, self.prim2cgto_norms, self.cgto_norms = None, None, None

    def label_cgtos(self):
        """Create string labels for each basis function (AO)"""
        bas_labels = []
        for i in range(self.nbas):
            c, n, l, m = self.bas_ids[i]
            c -= 1
            n += l  # looks like the shell number starts from 1?
            if l == 0:
                lbl = '{:5}{:3d}{:1}'.format(self.at_lbls[c], n, 's')
            elif l == 1:
                lbl = '{:5}{:3d}{:1}{:1}'.format(self.at_lbls[c], n, 'p', cartcomps[m])
            else:
                lbl = '{:5}{:3d}{:1}{:+}'.format(self.at_lbls[c], n, shells_by_angmom[l], m)
            bas_labels.append(lbl)

        return bas_labels

    @staticmethod
    def gto_norm(n, a):
        # normalization factor of function r^n e^{-a r^2}
        s = 2 ** (2 * int(n) + 3) * factorial(int(n) + 1) * (2 * a) ** (int(n) + 1.5) \
            / (factorial(2 * int(n) + 2) * sqrt(pi))
        return sqrt(s)

    def normalise_gto(self):
        self.gto_norms = np.array([self.gto_norm(self.prims[i, 1], self.prims[i, 2]) for i in range(self.nprimr)])

    def normalise_cgto(self):
        # THIS IS FOR UN-NORMALISED PRIMITIVES!
        cgto_norms = np.zeros(self.nbasr)
        idx = 0
        for k in range(self.nbasr):
            lk = self.cgtos[k, 1]  # angular momentum
            # Loop over all pairs of primitives in the CGTO expansion
            for i in range(self.cgto_terms[k]):
                for j in range(i):
                    cgto_norms[k] += 2 * self.expcoef[idx + i, 1] * self.expcoef[idx + j, 1] / \
                                     ((self.expcoef[idx + i, 0] + self.expcoef[idx + j, 0]) ** (lk + 1.5))
                # Add diagonal element
                cgto_norms[k] += self.expcoef[idx + i, 1] ** 2 / ((2 * self.expcoef[idx + i, 0]) ** (lk + 1.5))
            # Multiply by lk-dependent factor and take inverse square root of sum
            cgto_norms[k] = (sqrt(pi) * factorial(2 * int(lk) + 2) * cgto_norms[k] /
                             (2 ** (2 * lk + 3) * factorial(int(lk) + 1))) ** (- 0.5)
            # Move running index to the next block of CGTO contribs
            idx += self.cgto_terms[k]
        self.cgto_norms = cgto_norms

    def normalise_prim2cgto(self):
        # THIS IS FOR NORMALISED PRIMITIVES!
        cgto_norms = np.zeros(self.nbasr)
        idx = 0
        for k in range(self.nbasr):
            lk = self.cgtos[k, 1]  # angular momentum
            # Loop over all pairs of primitives in the CGTO expansion
            for i in range(self.cgto_terms[k]):
                for j in range(i):
                    cgto_norms[k] += 2 * self.expcoef[idx + i, 1] * self.expcoef[idx + j, 1] \
                                     * (sqrt(self.expcoef[idx + i, 0] * self.expcoef[idx + j, 0]) /
                                        (self.expcoef[idx + i, 0] + self.expcoef[idx + j, 0])) ** (lk + 1.5)
                # Add diagonal element
                cgto_norms[k] += self.expcoef[idx + i, 1] ** 2 * 0.5 ** (lk + 1.5)
            # Multiply by lk-dependent factor and take inverse square root of sum
            cgto_norms[k] = (2 ** (lk + 1.5) * cgto_norms[k]) ** (- 0.5)
            # Move running index to the next block of CGTO contribs
            idx += self.cgto_terms[k]
        self.prim2cgto_norms = cgto_norms

    def prim_to_cgto(self, matr_prim, unnorm_prim=False):
        """Converts all dimensions of matr_prim from a normalised primitive GTO basis
        to a CGTO basis using the information stored in the Basis
        object.
        unnorm_prim = flag to specify if primitives are assumed to be normalised"""
        # Find number of dimensions of matr_prim
        dimensions = len(matr_prim.shape)
        # Compute normalisation constants
        if unnorm_prim:
            self.normalise_cgto()
            norms = self.cgto_norms
        else:
            self.normalise_prim2cgto()
            norms = self.prim2cgto_norms
        # First, contract basis across rows
        matr_contr = matr_prim
        new_dimensions = [self.nprim] * dimensions
        for dim in range(dimensions):
            old_matr = matr_contr
            new_dimensions[dim] = self.nbas
            matr_contr = np.zeros(tuple(new_dimensions))
            for i in range(self.contr.shape[0]):
                at_i, li, shl_i, gto_i = self.contr[i, :].astype(int)
                # not sure astype is necessary since contr only has integers
                cgto_i = np.where(np.all(self.cgtos == self.contr[i, :3].reshape(1, 3), axis=1))
                # Shells in self.contr are 1-indexed, but shl_i needs to be 0-indexed
                shl_i -= 1
                (nshl_i, cidx) = self.aoshells[(at_i, li)]
                (npshl, pidx) = self.primshells[(at_i, li)]
                pshl = int(self.prims[gto_i, 3])
                # Create slices for array indexing
                prim_slice = [slice(None)] * dim + [slice(pidx + pshl, pidx + npshl * (2 * li + 1), npshl)] + \
                             [slice(None)] * (dimensions - dim - 1)
                contr_slice = [slice(None)] * dim + [slice(cidx + shl_i, cidx + nshl_i * (2 * li + 1), nshl_i)] + \
                              [slice(None)] * (dimensions - dim - 1)
                # Contract dimension
                matr_contr[tuple(contr_slice)] += old_matr[tuple(prim_slice)] * self.expcoef[i, 1] * norms[cgto_i]
        return matr_contr
