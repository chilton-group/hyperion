#!/usr/bin/env python3
import numpy as np
import opt_einsum as oe

from hyperion.electronic_structure import read_h5


class StateInteraction:
    def __init__(self, sih5, basis=None, get_ao_overlap=False):
        sih5 = str(sih5)
        # Create new Basis object if not provided
        if basis is None:
            from hyperion.electronic_structure.basis import Basis
            self.aobasis = Basis(sih5, get_overlap=get_ao_overlap)
        else:
            self.aobasis = basis

        h5data = read_h5.get_rassi(sih5)
        self.nsf = h5data['nsf']
        self.sf_smult = h5data['sf_smult']
        self.sf_1tdm = h5data['sf_1tdm']
        self.sf_spin1tdm = h5data['sf_spin1tdm']

        self.so_energies = h5data['so_energies']

        # Compute spin quantum numbers
        self.sf_S = [round(float(smult - 1) / 2, 1) for smult in self.sf_smult]

        socoef_real = h5data['socoef_real']
        socoef_imag = h5data['socoef_imag']
        self.nso = socoef_real.shape[0]
        # Create a single complex array of so coefficients
        self.so_coef = 1j * socoef_imag + socoef_real
        # lines are SO states, columns are SF+Spin
        # i.e. SO states are row vectors

        # Factors to WE-reduce TDMs (NOT the same as Molcas WE-TDMs, which have an extra minus sign)
        self.sf_wefactors = {}
        for i in range(self.nsf):
            s1 = self.sf_S[i]
            for j in range(i, self.nsf):
                s2 = self.sf_S[j]
                if s1 == 0 and s2 == 0:
                    self.sf_wefactors[(i, j)] = 1
                elif s1 == s2:
                    self.sf_wefactors[(i, j)] = 0.5 * np.sqrt((1 + s1) * (2 * s1 + 1) / s1)
                elif s2 == s1 + 1:
                    self.sf_wefactors[(i, j)] = 0.5 * np.sqrt((s1 + 1) * (2 * s1 + 3))
                elif s2 == s1 - 1:
                    self.sf_wefactors[(i, j)] = 0.5 * np.sqrt(s1 * (2 * s1 + 1))
                else:
                    self.sf_wefactors[(i, j)] = 0

    def ao2sf(self, matr_ao, dm='spin-summed'):
        """Contracts matrix in AO basis with transposed 1-body density matrix
        and (if nroots > 1) with 1-body transposed transition density matrices."""
        if dm == 'spin':
            tdms = self.sf_spin1tdm
        elif dm == 'we':
            tdms = np.zeros((self.nsf, self.nsf, self.aobasis.nbas, self.aobasis.nbas))
            for sf1 in range(self.nsf):
                for sf2 in range(sf1, self.nsf):
                    tdms[sf1, sf2] = self.sf_wefactors[(sf1, sf2)] * self.sf_spin1tdm[sf1, sf2]
        else:
            tdms = self.sf_1tdm

        matr_sf = np.zeros((self.nsf, self.nsf), dtype=matr_ao.dtype)
        for sf1 in range(self.nsf):
            for sf2 in range(sf1, self.nsf):
                matr_sf[sf1, sf2] = oe.contract('ij,ij->', tdms[sf1, sf2], matr_ao)
                if sf1 != sf2:
                    if np.iscomplexobj(matr_sf):
                        matr_sf[sf2, sf1] = matr_sf[sf1, sf2].conj()
                    else:
                        matr_sf[sf2, sf1] = matr_sf[sf1, sf2]
        # AO 1-TDMs between roots (sf1, sf2) with sf1 > sf2  are NOT computed by OpenMolcas
        # These matrix elements are 0 in self.sf_1tdm

        return matr_sf

    def sf2sfs(self, matr_sf):
        """Computes Kronecker product of identity matrix in |ms> basis
        with a matrix in the spin-free a.k.a CI roots basis (matr_sf).
        The resulting matrix (matr_sfs) is in the spin-free+spin basis."""
        # number of SF+spin states = number of SO states
        matr_sfs = np.zeros((self.nso, self.nso), dtype=complex)

        idx1 = 0
        for sf1 in range(self.nsf):
            mult1 = self.sf_smult[sf1]
            idx2 = 0
            for sf2 in range(self.nsf):
                mult2 = self.sf_smult[sf2]
                matr_sfs[idx1: idx1 + mult1, idx2: idx2 + mult2] = matr_sf[sf1, sf2] * np.eye(mult1, M=mult2)
                idx2 += mult2
            idx1 += mult1

        return matr_sfs

    def sf2sfs_spin(self, matr_sf):
        """Computes matrix representation of a mixed spin-spatial operator,
        using a RASSI-like formalism.
        Input:
            matr_sf - 3D numpy array corrsponding to the matrix representstion
                of a vector operator; matr_sf[0, :, :] is the x component,
                matr_sf[1, :, :] is y, matr_sf[2, :, :] is z;
                each component is a nsf x nsf matrix
        """
        # number of SF+spin states = number of SO states
        matr_sfs = np.zeros((self.nso, self.nso), dtype=complex)

        idx1 = 0
        for sf1 in range(self.nsf):
            s1 = round(float(self.sf_smult[sf1] - 1) / 2, 1)
            idx2 = idx1
            for sf2 in range(sf1, self.nsf):
                s2 = round(float(self.sf_smult[sf2] - 1) / 2, 1)
                for i1 in range(self.sf_smult[sf1]):
                    ms1 = i1 - s1
                    for i2 in range(self.sf_smult[sf2]):
                        ms2 = i2 - s2
                        if s1 == 0 and s2 == 0:  # matrix element between 2 singlets
                            matr_sfs[idx1 + i1, idx2 + i2] = matr_sf[2, sf1, sf2]
                        elif s1 == s2:
                            if ms1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = ms1 * matr_sf[2, sf1, sf2]
                            elif ms1 + 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = np.sqrt((s1 - ms1) * (s1 + ms1 + 1)) / 2 \
                                                                 * (matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            elif ms1 - 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = - np.sqrt((s1 + ms1) * (s1 - ms1 + 1)) / 2 \
                                                                 * (- matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            # Add common (positive) s1-dependent factor
                            matr_sfs[idx1 + i1, idx2 + i2] *= (s1 * (s1 + 1) * (2 * s1 + 1)) ** (- 0.5)
                        elif s1 + 1 == s2:
                            if ms1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = np.sqrt((s1 + 1) ** 2 - ms1 ** 2) \
                                                                 * matr_sf[2, sf1, sf2]
                            elif ms1 + 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = - np.sqrt((s1 + ms1 + 1) * (s1 + ms1 + 2)) / 2 \
                                                                 * (matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            elif ms1 - 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = - np.sqrt((s1 - ms1 + 1) * (s1 - ms1 + 2)) / 2 \
                                                                 * (- matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            # Add common (positive) s1-dependent factor
                            matr_sfs[idx1 + i1, idx2 + i2] *= ((s1 + 1) * (2 * s1 + 1) * (2 * s1 + 3)) ** (-0.5)
                        elif s1 - 1 == s2:
                            if ms1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = np.sqrt(s1 ** 2 - ms1 ** 2) * matr_sf[2, sf1, sf2]
                            elif ms1 + 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = np.sqrt((s1 - ms1) * (s1 - ms1 - 1)) / 2 \
                                                                 * (matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            elif ms1 - 1 == ms2:
                                matr_sfs[idx1 + i1, idx2 + i2] = np.sqrt((s1 + ms1) * (s1 + ms1 - 1)) / 2 \
                                                                 * (- matr_sf[0, sf1, sf2] + 1j * matr_sf[1, sf1, sf2])
                            # Add common (positive) s1-dependent factor
                            matr_sfs[idx1 + i1, idx2 + i2] *= (s1 * (2 * s1 - 1) * (2 * s1 + 1)) ** (-0.5)
                        # Set corresponding element in lower triangle
                        if sf1 != sf2:
                            matr_sfs[idx2 + i2, idx1 + i1] = \
                                np.conj(matr_sfs[idx1 + i1, idx2 + i2])
                idx2 += self.sf_smult[sf2]
            idx1 += self.sf_smult[sf1]

        return matr_sfs

    def sfspin2so(self, matr_sfs):
        return self.so_coef.conj() @ matr_sfs @ self.so_coef.T


def test_hso(siobj, file_real, file_imag):
    from ..tools.matrix_tools import matrix_from_file, matrix_to_file

    matr_real = matrix_from_file(file_real)
    matr_imag = matrix_from_file(file_imag)
    matr_sfs = 1j * matr_imag + matr_real
    matr_so = siobj.sfspin2so(matr_sfs)
    matrix_to_file(np.real(matr_so), filename='matr_so.real')
    matrix_to_file(np.imag(matr_so), filename='matr_so.imag')
    # Check with Molcas eigenvectors
    sovecs_real = matrix_from_file('molcas_HSO_vecs.real')
    sovecs_imag = matrix_from_file('molcas_HSO_vecs.imag')
    sovecs = 1j * sovecs_imag + sovecs_real
    checkmatr_so = sovecs.conj().T @ matr_sfs @ sovecs
    matrix_to_file(np.real(checkmatr_so), filename='checkmatr_so.real')
    matrix_to_file(np.imag(checkmatr_so), filename='checkmatr_so.imag')


def make_hso(h5file):
    from hyperion.tools.matrix_tools import matrix_to_file
    
    siobj = StateInteraction(h5file)
    amfi_ints = read_h5.get_dataset(h5file, 'SFS_AMFIINT')
    # updated to make AMFI ints imaginary anti-symmetric matrix
    amfi_ints_imag = 1j * amfi_ints
    amfi_ints_imag[1] *= -1  # y component already contains the sign change from i^2
    hso_sfs = siobj.sf2sfs_spin(amfi_ints_imag)
    matrix_to_file(np.real(hso_sfs), filename='HSO_hyperion.real')
    matrix_to_file(np.imag(hso_sfs), filename='HSO_hyperion.imag')
    matrix_to_file(read_h5.get_dataset(h5file, 'HSO_MATRIX_REAL'), filename='HSO_molcas.real')
    matrix_to_file(read_h5.get_dataset(h5file, 'HSO_MATRIX_IMAG'), filename='HSO_molcas.imag')


if __name__ == "__main__":
    import argparse
    # from tools.matrix_tools import matrix_to_file

    parser = argparse.ArgumentParser()
    parser.add_argument('h5file',
                        help='name of HDF5 file containing molecule info')
    args = parser.parse_args()
    
    make_hso(args.h5file)
