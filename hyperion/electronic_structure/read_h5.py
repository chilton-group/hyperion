#!/usr/bin/env python3
import numpy as np
from math import *
import h5py


def get_attribute(h5file, attrname):
    """General utility function for extracting a specified
    attribute attrname from hdf5 file h5file."""
    with h5py.File(str(h5file), 'r') as f:
        attrvalue = f.attrs.get(str(attrname))

    return attrvalue


def get_dataset(h5file, dsetname):
    """General utility function for extracting a specified
    dataset dsetname from hdf5 file h5file."""
    with h5py.File(str(h5file), 'r') as f:
        dsetdata = f.get(str(dsetname))[()]

    return np.array(dsetdata)


def get_rasorb(scfh5):
    """Reads and returns MO vectors from rasscf.h5 file."""
    with h5py.File(str(scfh5), 'r') as f:
        mo_vecs = f.get('MO_VECTORS')[()]
        # mo_vec is 1D, need to reshape it into a square matrix
        nbas = int(sqrt(mo_vecs.size))
        mo_vecs = mo_vecs.reshape(nbas, nbas)  # Each row in mo_vec is one MO

    return mo_vecs


def get_generalinfo(h5file, module):
    data = {}

    with h5py.File(str(h5file), 'r') as f:
        data['molcas_version'] = f.attrs.get('MOLCAS_VERSION').decode('UTF-8')
        if module == 'RASSCF':
            data['ci_type'] = f.attrs.get('CI_TYPE').decode('UTF-8')
            data['nactel'] = int(f.attrs.get('NACTEL'))
            data['nelec3'] = int(f.attrs.get('NELEC3'))
            data['nhole1'] = int(f.attrs.get('NHOLE1'))

    return data


def get_atomcentres(scfh5):
    data = {}

    with h5py.File(str(scfh5), 'r') as f:
        data['natoms'] = f.attrs.get('NATOMS_UNIQUE')
        data['at_charges'] = np.asarray(f.get('CENTER_CHARGES')[()], dtype=np.int16)
        data['at_coords'] = f.get('CENTER_COORDINATES')[()]
        # Coordinates are in Bohr, which is exactly what's needed for libcint
        labels = f.get('CENTER_LABELS')[()]

    center_lbls = []
    for l in labels:
        center_lbls.append(str(l.strip(), 'utf-8').capitalize())
    data['at_lbls'] = np.array(center_lbls, dtype=str)

    return data


def get_basis(scfh5, get_overlap=False):
    """Reads information about CGTO basis set from an OpenMolcas
    orbital HDF5 file. Note that the NPRIM dataset in the file is not
    actually the number of unique primitives, but rather the number of
    contracted function coefficients (nCGTO_coef)."""
    data = {}

    with h5py.File(str(scfh5), 'r') as f:
        data['expcoef'] = f.get('PRIMITIVES')[()]
        data['prim_ids'] = f.get('PRIMITIVE_IDS')[()]
        data['bas_ids'] = f.get('BASIS_FUNCTION_IDS')[()]
        [data['nbas']] = f.attrs.get('NBAS')  # Contracted basis size
        if get_overlap:
            data['ao_ovlp'] = f.get('AO_OVERLAP_MATRIX')[()]
            data['ao_ovlp'] = data['ao_ovlp'].reshape((data['nbas'], data['nbas']))
        else:
            data['ao_ovlp'] = None

    return data


def get_rasscf(scfh5, diagonal_only=False):
    """Reads and returns information about RASSCF states from rasscf.h5 file."""
    data = {}

    with h5py.File(str(scfh5), 'r') as f:
        # Read spin multiplicity
        data['spinmult'] = f.attrs.get('SPINMULT')

        # Read MO info
        data['mo_types'] = f.get('MO_TYPEINDICES')[()]  # read type indices
        data['mo_types'] = [str(s, 'utf-8') for s in data['mo_types']]
        nbas = len(data['mo_types'])
        # Compute ninact (inactive orbs) and nact (active orbitals)
        data['ninact'] = 0
        data['nact'] = 0
        for i in range(nbas):
            if data['mo_types'][i] == 'I':
                data['ninact'] += 1
            elif data['mo_types'][i] in ['1', '2', '3']:
                data['nact'] += 1

        data['mo_vecs'] = f.get('MO_VECTORS')[()]
        # mo_vec is 1D, need to reshape it into a square matrix
        data['mo_vecs'] = data['mo_vecs'].reshape(nbas, nbas)  # Each row in mo_vec is one MO

        data['nroots'] = int(f.attrs.get('NROOTS'))

        # Read density matrices
        allrdm = f.get('DENSITY_MATRIX')[()]
        data['rdms'] = {}
        allspindm = f.get('SPINDENSITY_MATRIX')[()]
        data['spindms'] = {}
        for rt in range(data['nroots']):
            data['rdms'][rt] = allrdm[rt, :, :]
            data['spindms'][rt] = allspindm[rt, :, :]

        if (not diagonal_only) and data['nroots'] > 1:
            try:
                alltdm = f.get('TRANSITION_DENSITY_MATRIX')[()]
            except TypeError:  # h5py raises TypeError when a dataset isn't found
                raise Exception(
                    'Error encountered while reading TRANSITION_DENSITY_MATRIX from hdf5 file.'
                    'Ensure that the OpenMolcas RASSCF input includes the TDM keyword.'
                )

            data['tdms'] = {}
            allspintdm = f.get('TRANSITION_SPIN_DENSITY_MATRIX')[()]
            data['spintdms'] = {}
            ridx = 0
            for rt1 in range(1, data['nroots']):
                for rt2 in range(rt1):
                    data['tdms'][(rt2, rt1)] = alltdm[ridx, :, :]
                    data['spintdms'][(rt2, rt1)] = allspintdm[ridx, :, :]
                    ridx += 1
        else:
            data['tdms'] = None
            data['spintdms'] = None

    return data


def get_rassi(sih5):
    """Reads and returns information about RASSI/MPSSI states from rassi.h5/mpssi.h5 file
    Note on naming convention:
        sf = spin-free state; so = spin-orbit state"""
    data = {}

    with h5py.File(str(sih5), 'r') as f:
        data['nsf'] = int(f.attrs.get('NSTATE'))  # number of spin-free states
        data['sf_smult'] = f.attrs.get('STATE_SPINMULT')[()]

        data['sf_1tdm'] = f.get('SFS_TRANSITION_DENSITIES')[()]
        # If 1tdm matrix elements are all 0, (spin-)TDMs weren't computed, so raise Exception
        if np.all((data['sf_1tdm'] == 0)):
            raise Exception(
                'Error encountered while reading SFS_TRANSITION_DENSITIES from hdf5 file'
                'Ensure that the OpenMolcas RASSI/MPSSI input includes the TRD1 keyword.'
            )
        data['sf_spin1tdm'] = f.get('SFS_TRANSITION_SPIN_DENSITIES')[()]
        # Reshape 1TDM array to 4D (nsfs, nsfs, nbas, nbas)
        nbas = int(sqrt(data['sf_1tdm'].shape[2]))
        data['sf_1tdm'] = data['sf_1tdm'].reshape((data['nsf'], data['nsf'], nbas, nbas))
        data['sf_spin1tdm'] = data['sf_spin1tdm'].reshape((data['nsf'], data['nsf'], nbas, nbas))

        data['socoef_real'] = f.get('SOS_COEFFICIENTS_REAL')[()]
        data['socoef_imag'] = f.get('SOS_COEFFICIENTS_IMAG')[()]

        data['so_energies'] = f.get('SOS_ENERGIES')[()] # get SO state energies for pNMR

    return data


def get_uhf(uhfh5):
    """Reads and returns information from scf.h5 file resulting from a UHF calculation.
    Assume UHF was done always, because there's no point running Hyperion on a closed shell system."""
    data = {}

    with h5py.File(str(uhfh5), 'r') as f:
        calc_type = f.attrs.get('ORBITAL_TYPE')
        # Catch wrong input files
        assert (b'UHF' in calc_type), "Hyperion only supports scf.h5 files from UHF or UKS calculations."

        data['alpha_occ'] = f.get('MO_ALPHA_OCCUPATIONS')[()]
        data['alpha_movecs'] = f.get('MO_ALPHA_VECTORS')[()]
        data['beta_occ'] = f.get('MO_BETA_OCCUPATIONS')[()]
        data['beta_movecs'] = f.get('MO_BETA_VECTORS')[()]

    nbas = data['alpha_occ'].size
    data['alpha_movecs'] = data['alpha_movecs'].reshape(nbas, nbas)
    data['beta_movecs'] = data['beta_movecs'].reshape(nbas, nbas)

    return data


if __name__ == "__main__":
    import argparse
    from hyperion.tools.matrix_tools import matrix_to_file

    # Parse command line arguments
    parser = argparse.ArgumentParser()
    # parser.add_argument('rasscfh5', help = 'RASSCF HDF5 file from OpenMolcas')
    parser.add_argument('rassih5', help='RASSI HDF5 file from OpenMolcas')
    args = parser.parse_args()

    '''
    (spinmult, mo_vecs, rdms, spindms, tdms, spintdms, ninact, nact, nroots) = \
            get_rasscf(args.rasscfh5)
    s = round(float(spinmult - 1) / 2, 1)
    scftdm = {}
    for i in spindms.keys():
        full_spindm = np.zeros(mo_vecs.shape)
        full_spindm[ninact : ninact + nact, ninact : ninact + nact] = spindms[i]
        scftdm[i] = mo_vecs @ full_spindm @ mo_vecs.T
        scftdm[i] *= -0.5 * sqrt((s + 1) * (2 * s + 1) / s)
        matrix_to_file(scftdm[i], filename='scfwetdm_' + str(i))
    '''

    nsf, sf_smult, sf_1tdm, sf_spin1tdm, socoef_real, socoef_imag = get_rassi(args.rassih5)
    nbas = sf_spin1tdm.shape[2]
    sf_we1tdm = get_dataset(args.rassih5, 'SFS_WE_TRANSITION_DENSITIES')
    sf_we1tdm = sf_we1tdm.reshape((nsf, nsf, nbas, nbas))
    for j in range(nsf):
        for i in range(j + 1):
            spindm = sf_spin1tdm[i, j]
            wedm = sf_we1tdm[i, j]
            matrix_to_file(spindm, filename='spindm_' + str(i) + '_' + str(j))
            matrix_to_file(wedm, filename='wedm_' + str(i) + '_' + str(j))
