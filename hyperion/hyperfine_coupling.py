#!/usr/bin/env python3
import numpy as np
import opt_einsum as oe
import re

from hyperion.libcint_interface import LibcintInterface
from hyperion.analysis.hfc_orbdec import HFCOrbDec
import hyperion.analysis.tensor_util as tenstools
import hyperion.tools.matrix_tools as mtools
import hyperion.tools.constants as const
from hyperion.tools.output_util import write_tensordata, write_hfcinfo, write_signtable


class HyperfineCoupling:
    def __init__(self, wavefunction, spinonly=False, args=None, cinter=None, verbose=True):
        self.spinonly = spinonly
        self.rasscf_mode, self.uhf_mode = False, False
        # Bools for "special" modes; RASSI mode (symmetrised HFC tensor method) is considered default
        if type(wavefunction).__name__ == 'FockSpace':
            self.rasscf_mode = True  # used to determine what functions to call
        elif type(wavefunction).__name__ == 'UHF':
            self.uhf_mode = True

        if args is not None and args.orbdec is not None:
            self.do_orbdec = True
        else:
            self.do_orbdec = False

        # Store wavefunction object - instance of class FockSpace (RASSCF), StateInteraction (RASSI) or UHF (UHF/UKS)
        self.wfn = wavefunction

        # Set up HFC calculation options
        if args is not None:
            self.nopc = args.nopc
        else:
            self.nopc = False

        if self.rasscf_mode or self.uhf_mode:
            self.nosoc = True
        elif args is not None:
            # used to determine what matrix representation to use when there are 2 sets of states (sfs and so)
            self.nosoc = args.nosoc
        else:
            self.nosoc = False

        if self.spinonly or self.uhf_mode or args is None:
            self.do_relative_signs = False
        else:
            self.do_relative_signs = args.relsign
        
        if args is not None and args.export is not None:
            self.do_export = True
            self.export_data = []
        else:
            self.do_export = False
            self.export_data = None

        # Write HFC section header and description to output
        if verbose is True: write_hfcinfo(spinonly=(self.spinonly or self.uhf_mode))
        # technically the approach is spin-only for spin-adapted wavefunctions alone
        # however, the HFC tensor formula printed by write_hfcinfo holds for both true spin-only and UHF

        # Create instance of HFCOrbDec class
        if self.do_orbdec:
            self.orbdec = HFCOrbDec(
                self.wfn,
                orbdec_roots=args.orbdec,
                orbdec_min=args.orbdec_min,
                orbdec_nlevels=args.orbdec_nlevels,
                guess_file=args.guess,
                noplot=args.noplot,
                orblabel_fmt=args.orbdec_labelfmt
            )

        # Set pseudospin multiplicities for which HFC is computed
        if self.spinonly or self.uhf_mode:
            self.pseudos_mult = None
        elif args is None or args.mult is None:
            self.pseudos_mult = [self.wfn.sf_smult[0]]
            # this sets the pseudoS multiplicity = spin multiplicity
            # of the first spin-free state passed to RASSI/MPSSI
        else:
            self.pseudos_mult = [int(mlt) for mlt in args.mult]

        # Read X2C transformation matrix blocks
        self.x2c_uu, self.x2c_lu = None, None
        if not self.nopc:
            uufile = 'X2C_Uuu'
            lufile = 'X2C_Ulu'
            try:
                self.x2c_uu = mtools.matrix_from_file(uufile)  # UU block
            except FileNotFoundError:
                raise Exception(
                    'X2C transformation file {} was not found in the current directory'.format(uufile)
                )
            try:
                self.x2c_lu = mtools.matrix_from_file(lufile)  # LU block
            except FileNotFoundError:
                raise Exception(
                    'X2C transformation file {} was not found in the current directory'.format(lufile)
                )

        # Set attribute pointing to Libcint interface
        self.cinter = cinter

        # Initialise attributes identifying the magnetic nucleus (change with every atom)
        self.nuc_mass, self.nuc_element, self.nuc_atidx, self.isotope, self.au2mhz = None, None, None, None, None

        # Initialise attributes holding spatial integrals and HFC operator matrices (change with every atom)
        self.ints_contr, self.h1fcsd, self.h1pso, self.h1 = None, None, None, None

        # Initialise attributes holding tensor data (change with every atom AND multiplet)
        self.aat, self.atens, self.hfcc, self.axes_hfc, self.eulang_hfc = None, None, None, None, None
        # The following attributes aren't used if employing a spin-only parametrisation
        self.aat_fcsd, self.hfcc_fcsd, self.axes_fcsd, self.eulang_fcsd = None, None, None, None
        self.aat_pso, self.hfcc_pso, self.axes_pso, self.eulang_pso = None, None, None, None
        self.relsign_pso, self.relsign_hfc = None, None

    def find_nucleus(self, nuc):
        # Parse nuc, get atom label and isotope label
        m = re.match('([0-9]*)([a-zA-z]+)([0-9]*)', nuc)
        elem = m.group(2).capitalize()
        if len(m.group(1)) == 0:  # isotope mass not specified
            # Look for most abundant magnetic isotope
            isotopes = const.isotopes[elem]
            isomass = 0
            abund = 0
            for imass in isotopes:
                iso = str(imass) + elem
                if const.nuc_spin[iso] != 0 and const.nuc_abundance[iso] > abund:
                    isomass = imass
                    abund = const.nuc_abundance[iso]
            if isomass == 0:
                raise ValueError('No magnetic isotopes were found for ' + elem)
        else:
            isomass = int(m.group(1))

        if len(m.group(3)) == 0:  # atom ID not specified
            atidx = 1
        else:
            atidx = int(m.group(3))
        self.nuc_mass, self.nuc_element, self.nuc_atidx = isomass, elem, atidx
        self.isotope = str(self.nuc_mass) + self.nuc_element

        # Print nucleus information
        if const.nuc_spin[self.isotope] == int(const.nuc_spin[self.isotope]):
            print('** ATOM {}{}, isotope {}, I = {:.0f} **'.format(self.nuc_element, self.nuc_atidx, self.isotope,
                                                                   const.nuc_spin[self.isotope]))
        else:
            print('** ATOM {}{}, isotope {}, I = {:.0f}/2 **'.format(self.nuc_element, self.nuc_atidx, self.isotope,
                                                                     2 * const.nuc_spin[self.isotope]))

    def spatial_integrals(self, atom=None, zeta=None):
        """Compute spatial part of the required integrals, picture-change transform if required, then
        convert to contracted basis. The final matrix is stored in self.ints_contr"""
        # Create Libcint interface if not already done
        if self.cinter is None:
            self.cinter = LibcintInterface()
            self.cinter.create_input(self.wfn.aobasis)
            # assume input was created if cinter exists
        # Set origin for hyp integrals
        if atom is not None:
            atid = np.where(self.wfn.aobasis.at_lbls == atom)[0][0]
            self.cinter.change_rinv_origin(atid)
        if zeta is not None and zeta != 1:
            self.cinter.set_rinv_zeta(zeta)
        # Compute hyperfine integrals in normalised primitive basis
        ints_prim = self.cinter.int1e_sph('cint1e_hyp_sph', 9)

        # X2C transform primitive array (if nopc is False)
        if not self.nopc:
            ints_prim = oe.contract('ia,kij->kaj', self.x2c_uu, ints_prim)
            ints_prim = oe.contract('kaj,jb->kab', ints_prim, self.x2c_lu)

        # Contract basis
        self.ints_contr = np.zeros((9, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        for k in range(9):
            self.ints_contr[k] = self.au2mhz * self.wfn.aobasis.prim_to_cgto(ints_prim[k])

    def hermitian_ints(self, hypints):
        """Function that uses the matrix representation of Kronecker-product
        -(nabla 1/r_N) nabla (1st dimension of hypints is the tensor component,
        2nd and 3rd dim = nbas) to construct Hermitian operator matrices
        (hyp_fcsd, used to compute the Fermi-contact and spin-dipole contributions)
        and an anti-Hermitian combination of the cross-product integrals (hyp_pso,
        used to evaluate the paramagnetic spin-orbit contribution)."""
        hyp_tens = hypints + hypints.transpose((0, 2, 1))
        hyp_tens = hyp_tens.reshape((3, 3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        # 1st index in reshaped array is vector field (nabla-rinv) component
        # 2nd index is the component of nabla acting on an orbital

        hyp_fcsd = np.zeros((3, 3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        hyp_fcsd += hyp_tens
        for k in range(3):
            hyp_fcsd[k, k] -= hyp_tens[0, 0] + hyp_tens[1, 1] + hyp_tens[2, 2]

        hyp_pso = np.zeros((3, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        hyp_pso[0] = hypints[5] - hypints[7]
        hyp_pso[1] = hypints[6] - hypints[2]
        hyp_pso[2] = hypints[1] - hypints[3]
        hyp_pso = 1j * (hyp_pso.transpose((0, 2, 1)) - hyp_pso)
        return hyp_fcsd, hyp_pso

    def h1_rasscf(self):
        """Determines hyperfine perturbation operators for a RASSCF wavefunction.
        Final values are in Mhz"""
        # Convert hyperfine integrals to MO basis
        hypint_mo = np.zeros((9, self.wfn.aobasis.nbas, self.wfn.aobasis.nbas))
        for k in range(9):
            hypint_mo[k] = self.wfn.ao2mo(self.ints_contr[k])
        # Hermitian operators are needed before contracting with 1-TDMs
        fcsd_mo, pso_mo = self.hermitian_ints(hypint_mo)
        # add factor of 2 from converting Pauli matrices to spin operators
        fcsd_mo *= 2

        # Do orbital decomposition if requested
        if self.do_orbdec:
            ninact, nact = self.wfn.ninact, self.wfn.nact
            # Pre-multiply spatial integral array by 1/S
            self.orbdec.analysis_rasscf(
                (1 / self.wfn.S) * fcsd_mo[2, :, ninact: ninact + nact, ninact: ninact + nact],
                self.wfn.spindms,
                self.isotope + str(self.nuc_atidx)
            )

        if self.spinonly:
            fcsd_sf = np.zeros((3, 3, 1, self.wfn.nroots))
            for k in range(3):
                for l in range(3):
                    fcsd_sf[k, l] = 0.5 * self.wfn.mo2ci(fcsd_mo[k, l], dm='spin', diagonal_only=True)
            # Only computing FC+SD term in spinonly mode
            self.h1fcsd = fcsd_sf.transpose((1, 0, 2, 3)).reshape((3, 3, self.wfn.nroots))
            # 1st index of fcsd_sf couples with S, 2nd dimension couples with I
            # this code uses the I a S spin Hamiltonian, therefore need to transpose tensor components of fcsd_sf
            self.h1 = self.h1fcsd
            # NOTE: in spinonly mode, the first 2 dimensions of h1fcsd are tensor components and the third is nroots
            # each self.h1fcsd[:, k, :] slice is obtained by assuming spin quantisation is along k
        else:
            # MO basis -> SF state (CI roots) basis: contract integrals with 1-TDMs; resulting matrices are WE-reduced
            fcsd_sf = np.zeros((3, 3, self.wfn.nroots, self.wfn.nroots))
            for k in range(3):
                for l in range(3):
                    fcsd_sf[k, l] = self.wfn.mo2ci(fcsd_mo[k, l], dm='we')
            pso_sf = np.zeros((3, self.wfn.nroots, self.wfn.nroots), dtype=complex)
            for k in range(3):
                pso_sf[k] = self.wfn.mo2ci(pso_mo[k])

            self.h1fcsd = np.zeros((3, self.wfn.nsfspin, self.wfn.nsfspin), dtype=complex)
            self.h1pso = np.zeros((3, self.wfn.nsfspin, self.wfn.nsfspin), dtype=complex)
            for k in range(3):
                self.h1pso[k] = self.wfn.ci2sfs(pso_sf[k])
                self.h1fcsd[k] = self.wfn.ci2sfs_spin(fcsd_sf[:, k])

            self.h1 = self.h1fcsd + self.h1pso

    def h1_rassi(self):
        """Determines hyperfine perturbation operators for a RASSI wavefunction."""
        # (anti-)Hermitian operators are needed before contracting with 1-TDMs
        fcsd_ao, pso_ao = self.hermitian_ints(self.ints_contr)
        # Add factor of 2 from converting Pauli matrices to spin operators
        fcsd_ao *= 2

        # AO basis -> SF state basis: contract integrals with 1-TDMs
        fcsd_sf = np.zeros((3, 3, self.wfn.nsf, self.wfn.nsf))
        for k in range(3):
            for l in range(3):
                fcsd_sf[k, l] = self.wfn.ao2sf(fcsd_ao[k, l], dm='we')

        pso_sf = np.zeros((3, self.wfn.nsf, self.wfn.nsf), dtype=complex)
        for k in range(3):
            pso_sf[k] = self.wfn.ao2sf(pso_ao[k])

        h1fcsd_sfs = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
        h1pso_sfs = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
        for k in range(3):
            h1pso_sfs[k] = self.wfn.sf2sfs(pso_sf[k])
            h1fcsd_sfs[k] = self.wfn.sf2sfs_spin(fcsd_sf[:, k])

        if self.nosoc:
            self.h1fcsd, self.h1pso = h1fcsd_sfs, h1pso_sfs
        else:
            # Convert perturbation operator h1_sfs to SO basis
            self.h1fcsd = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
            self.h1pso = np.zeros((3, self.wfn.nso, self.wfn.nso), dtype=complex)
            for k in range(3):
                self.h1fcsd[k] = self.wfn.sfspin2so(h1fcsd_sfs[k])
                self.h1pso[k] = self.wfn.sfspin2so(h1pso_sfs[k])

        self.h1 = self.h1fcsd + self.h1pso

    @staticmethod
    def diag_tensor(h1, spin):
        """Constructs and diagonalises the HFC tensor a
        Returns eigenvalues, eigenvectors and the symmetrised tensor"""
        atens = h1 / spin
        if (np.abs(atens) < 1e-16).all():
            # don't bother diagonalising tensors that are basically 0
            hfcc, axes_hfc = np.zeros(3, dtype=float), np.eye(3, dtype=float)
        else:
            hfcc, axes_hfc = np.linalg.eigh(atens)
        return hfcc, axes_hfc, atens

    @staticmethod
    def diag_symmtensor(h1, spin):
        """Constructs and diagonalises the symmetrised HFC tensor A = a a^T
        Returns eigenvalues, eigenvectors and the symmetrised tensor"""
        aat = np.real(oe.contract('kij,lji->kl', h1, h1))
        aat *= float(3) / (spin * (spin + 1) * (2 * spin + 1))

        if (aat < 1e-16).all():
            # don't bother diagonalising tensors that are basically 0
            hfcc2, axes_hfc = np.zeros(3, dtype=float), np.eye(3, dtype=float)
        else:
            hfcc2, axes_hfc = np.linalg.eigh(aat)
            if not (hfcc2 >= 0).all():
                print('Warning! Negative eigenvalues found for the symmetrised HFC tensor.')

        return hfcc2, axes_hfc, aat

    def write_results(self, mult_id, seff):
        if self.uhf_mode:
            print(' * Multiplet {}, S = {:.3f}\n'.format(mult_id + 1, seff))
        elif self.spinonly:
            print(' * Spin multiplet {}, S = {:.1f}\n'.format(mult_id + 1, seff))
        else:
            print(' * Pseudospin multiplet {}, S = {:.1f}\n'.format(mult_id + 1, seff))

        if seff == 0:
            parametrisation = None
            print('  No hyperfine coupling computed for singlet state.')
        elif self.spinonly or self.uhf_mode:
            parametrisation = 'spin-only'
            write_tensordata(self.atens,
                             self.hfcc,
                             self.axes_hfc,
                             angles=self.eulang_hfc,
                             header1='  Fermi contact (FC) + spin-dipole (SD) HFC tensor (MHz):',
                             header2='  FC+SD hyperfine coupling constants (MHz), main axes and Euler angles:',
                             evalname='A')
        else:
            parametrisation = 'pseudospin'
            # Write out data for FC+SD, PSO and total tensors
            write_tensordata(self.aat_fcsd,
                             self.hfcc_fcsd,
                             self.axes_fcsd,
                             angles=self.eulang_fcsd,
                             header1='  Fermi contact (FC) + spin-dipole (SD) symmetrised HFC tensor (MHz^2):',
                             header2='  FC+SD hyperfine coupling constants (MHz), main axes and Euler angles:',
                             evalname='A')

            write_tensordata(self.aat_pso,
                             self.hfcc_pso,
                             self.axes_pso,
                             angles=self.eulang_pso,
                             header1='  Paramagnetic spin-orbit (PSO) symmetrised HFC tensor (MHz^2):',
                             header2='  PSO hyperfine coupling constants (MHz), main axes and Euler angles:',
                             evalname='A')

            write_tensordata(self.aat,
                             self.hfcc,
                             self.axes_hfc,
                             angles=self.eulang_hfc,
                             header1='  Full (FC+SD+PSO) symmetrised HFC tensor (MHz^2):',
                             header2='  Hyperfine coupling constants (MHz), main axes and Euler angles of the full HFC '
                                     'tensor:',
                             evalname='A')

            if self.do_relative_signs:
                write_signtable(self.relsign_pso,
                                self.relsign_hfc,
                                partnames=['FC+SD', 'PSO', 'Total'],
                                evalname='A')

        print()

        if self.do_export and seff != 0:
            temp_data = [
                'HyperfineCoupling',
                mult_id + 1,
                seff,
                parametrisation,
                self.isotope,
                '{}{}'.format(self.nuc_element, self.nuc_atidx),
                const.nuc_spin[self.isotope]
            ]
            if self.spinonly or self.uhf_mode:
                # Fill the PSO columns with NaNs
                temp_data += [
                    *self.hfcc_fcsd,
                    *self.eulang_fcsd,
                    np.nan, np.nan, np.nan,
                    np.nan, np.nan, np.nan,
                    *self.hfcc,
                    *self.eulang_hfc
                ]
            else:
                temp_data += [
                    *self.hfcc_fcsd,
                    *self.eulang_fcsd,
                    *self.hfcc_pso,
                    *self.eulang_pso,
                    *self.hfcc,
                    *self.eulang_hfc
                ]
            if self.do_relative_signs:
                temp_data += [
                    *self.relsign_pso,
                    *self.relsign_hfc
                ]

            self.export_data.append(temp_data)

    def hfc_uhf(self):
        fcsd_ao, pso_ao = self.hermitian_ints(self.ints_contr)
        # Add factor of 2 to FC+SD part
        fcsd_ao *= 2

        # Do orbital decomposition
        if self.do_orbdec:
            self.orbdec.analysis_uhf(
                (1 / self.wfn.Sz) * fcsd_ao[2, :, :, :],
                self.wfn.spindm,
                self.isotope + str(self.nuc_atidx)
            )

        self.h1fcsd = 0.5 * oe.contract('ij,abij->ba', self.wfn.spindm, fcsd_ao)
        # 1st index of h1fcsd couples with S, 2nd dimension couples with I
        # this code uses the I a S spin Hamiltonian, therefore need to transpose h1fcsd by swapping a and b
        self.hfcc, self.axes_hfc, self.atens = self.diag_tensor(self.h1fcsd, self.wfn.Sz)
        self.eulang_hfc = tenstools.rotation_to_euler_angles(self.axes_hfc)
        self.hfcc_fcsd, self.axes_fcsd, self.eulang_fcsd = self.hfcc, self.axes_hfc, self.eulang_hfc

        self.write_results(0, self.wfn.Sz)

    def hfc_spinonly(self):
        self.h1_rasscf()

        for rt in range(self.wfn.nroots):
            self.hfcc, self.axes_hfc, self.atens = self.diag_tensor(self.h1fcsd[:, :, rt], self.wfn.S)
            self.eulang_hfc = tenstools.rotation_to_euler_angles(self.axes_hfc)
            self.hfcc_fcsd, self.axes_fcsd, self.eulang_fcsd = self.hfcc, self.axes_hfc, self.eulang_hfc
            self.write_results(rt, self.wfn.S)

    def hfc_pseudospin(self):
        if self.rasscf_mode:
            self.h1_rasscf()
        else:
            self.h1_rassi()

        idx = 0
        for mult_id, mult in enumerate(self.pseudos_mult):
            s = round(float(mult - 1) / 2, 1)
            if s == 0:  # skip singlets - no HFC
                self.write_results(mult_id, s)
                idx += mult
                continue

            # FC + SD contribution
            self.hfcc_fcsd, self.axes_fcsd, self.aat_fcsd = self.diag_symmtensor(self.h1fcsd[:, idx: idx + mult,
                                                                                 idx: idx + mult], s)
            self.hfcc_fcsd = np.sqrt(np.abs(self.hfcc_fcsd))

            # PSO contribution
            self.hfcc_pso, self.axes_pso, self.aat_pso = self.diag_symmtensor(self.h1pso[:, idx: idx + mult,
                                                                              idx: idx + mult], s)
            self.hfcc_pso = np.sqrt(np.abs(self.hfcc_pso))

            # Total (FC + SD + PSO) hyperfine coupling tensor, eigenvalues and eigenvectors
            self.hfcc, self.axes_hfc, self.aat = self.diag_symmtensor(self.h1[:, idx: idx + mult, idx: idx + mult], s)
            self.hfcc = np.sqrt(np.abs(self.hfcc))

            # Reorder FC+SD and PSO HFCCs and axes to maximise overlap with main axes of complete HFC tensor
            reorder_fcsd = tenstools.reorder_by_overlap(self.axes_fcsd, self.axes_hfc)
            self.hfcc_fcsd = self.hfcc_fcsd[reorder_fcsd]
            self.axes_fcsd = self.axes_fcsd[:, reorder_fcsd]

            reorder_pso = tenstools.reorder_by_overlap(self.axes_pso, self.axes_hfc)
            self.hfcc_pso = self.hfcc_pso[reorder_pso]
            self.axes_pso = self.axes_pso[:, reorder_pso]

            # Compute Euler angles
            self.eulang_fcsd = tenstools.rotation_to_euler_angles(self.axes_fcsd)
            self.eulang_pso = tenstools.rotation_to_euler_angles(self.axes_pso)
            self.eulang_hfc = tenstools.rotation_to_euler_angles(self.axes_hfc)

            # Relative signs of PSO and total eigenvalues
            if self.do_relative_signs:
                self.relsign_pso, self.relsign_hfc = tenstools.relative_sign_by_magnitude([self.hfcc_fcsd,
                                                                                           self.hfcc_pso,
                                                                                           self.hfcc])

            self.write_results(mult_id, s)

            # Increment idx to go to the next multiplet
            idx += mult

    def compute_hfc(self, nuc, zeta=None):
        self.find_nucleus(nuc)

        # Constant for converting a.u. (Bohr radii^(-3)) -> MHz
        self.au2mhz = const.mu0_4pi * const.mu_nuc * const.nuc_g[self.isotope] \
                      * const.mu_bohr * const.bohr_rad ** (-3) \
                      / const.hplanck * 10 ** (-6)

        # Print special settings
        if zeta is not None:
            print(' ...modelling the nuclear magnetisation as a finite Gaussian distribution with')
            print('    zeta = ' + str(zeta))

        # Compute HFC spatial integrals for selected nucleus
        self.spatial_integrals(atom=self.nuc_element + str(self.nuc_atidx), zeta=zeta)

        # Call electronic-structure-specific HFC methods
        if self.uhf_mode:
            self.hfc_uhf()
        elif self.spinonly:
            self.hfc_spinonly()
        else:
            self.hfc_pseudospin()


    def compute_h1(self, nuc, zeta=None):
        self.find_nucleus(nuc)

        # Constant for converting a.u. (Bohr radii^(-3)) -> MHz
        self.au2mhz = const.mu0_4pi * const.mu_nuc * const.nuc_g[self.isotope] \
                      * const.mu_bohr * const.bohr_rad ** (-3) \
                      / const.hplanck * 10 ** (-6)

        # Compute HFC spatial integrals for selected nucleus
        self.spatial_integrals(atom=self.nuc_element + str(self.nuc_atidx), zeta=zeta)

        # Call electronic-structure-specific HFC methods
        if self.rasscf_mode:
            self.h1_rasscf()
        else:
            self.h1_rassi()
