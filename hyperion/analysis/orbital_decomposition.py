#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import lines, cm, colors, gridspec
from matplotlib.colorbar import Colorbar
from math import pi, floor


def array_to_cmap(values, cmap='viridis_r'):
    """Assigns colours from a colormap based on the elements of values (numpy array).
    Returns a ScalarMappable instance and a list of (R,G,B,A) tuples, one tuple for each element of values."""
    colmap = plt.get_cmap(cmap)
    nonzero_min = np.amin(values[np.nonzero(values)])
    norm = colors.LogNorm(vmin=nonzero_min, vmax=np.amax(values))
    mapper = cm.ScalarMappable(norm=norm, cmap=colmap)
    col_tuples = []
    for i in range(values.size):
        if values[i] == 0:
            col_tuples.append(colmap(0))  # make any zeroes white
        else:
            col_tuples.append(mapper.to_rgba(values[i]))
    return mapper, col_tuples


def orbdec_diagram(norb, maxthr_power, e_dict, alpha_dict, e_diag,
                   unit=None, labels=None, label_colours=None,
                   save=False, fname=None, fformat=None):
    """Plots an orbital decomposition diagram from a 2D matrix ("entanglement" matrix, e_mat)
    Input:
        norb: number of orbitals plotted
        maxthr_power: maximum order of magnitude threshold
        e_dict: dictionary of e_mat matrix element indices (i, j), where the key is the order of magnitude
        alpha_dict: dict of transparency values for an orbital pair (i, j) corresponding to a
                   line in the orbital decomposition diagram
        e_diag: vector of length norb containing total orbital contributions to the e_mat matrix
        labels: list of strings to use as orbital labels
        save: True to save the diagram to a file
        fname: name of the file (minus extension) to save the diagram as
        fformat: file format to save as; WARNING: transparency is not supported for files saved as .eps
    """
    # Current colour scheme
    # royal blue, magenta, teal, red, Kermit green, dusty blue, highlighter yellow
    colour_list = ['#001eff', '#bd00ff', '#00ff9f', '#da1631', '#bada55', '#6897bb', '#ffff00']

    if save:
        if fname is None:
            fname = 'orbdec_diagram'
        if fformat is None:
            fformat = 'svg'

    if labels is None:
        labels = [str(i) for i in range(1, norb + 1)]

    if label_colours is None:
        label_colours = ['#000000'] * norb

    theta = np.zeros(norb)
    r = np.zeros(norb)
    slice_ = - 2 * pi / norb
    for i in range(norb):
        theta[i] = i * slice_ + pi / 2 + slice_ / 2
        r[i] = 1.0

    fig = plt.figure(figsize=(8, 6))
    gs = gridspec.GridSpec(1, 2, width_ratios=[50, 1])
    ax1 = fig.add_subplot(gs[0], polar=True)  # polar subplot for orbdec diagram
    ax2 = fig.add_subplot(gs[1])  # colorbar for total orbital contributions

    ax1.set_xticklabels([])
    ax1.set_yticklabels([])
    ax1.grid(b=False)

    # First plot lines corresponding to pairs of orbitals
    legendlines = {}
    for thr_power in sorted(e_dict.keys()):
        # re-loop over colour list if plotting more levels
        colour_idx = (maxthr_power - thr_power) % len(colour_list)

        for (i, j) in e_dict[thr_power]:
            x = [theta[i], theta[j]]
            y = [1, 1]
            line = lines.Line2D(
                x, y,
                linewidth=1, linestyle='-',
                alpha=alpha_dict[(i, j)],
                color=colour_list[colour_idx],
            )
            ax1.add_line(line)

        # Use a dummy opaque line for the legend
        if unit is None:
            lbl = r'10$^{' + str(thr_power) + '}$'
        else:
            lbl = r'10$^{' + str(thr_power) + '}$ ' + str(unit)

        line = lines.Line2D(
            [0, 0], [1, 1],
            linewidth=1, linestyle='-', alpha=1, label=lbl,
            color=colour_list[colour_idx]
        )

        legendlines[lbl] = line

    # Plot orbital labels and edge circles
    # Vary circle area based on norb
    if norb <= 40:
        sarea = 50
        textsize = 'small'
    else:
        sarea = 40
        textsize = 'x-small'

    mapper, col_tuples = array_to_cmap(e_diag, cmap='viridis_r')
    _ = ax1.scatter(theta, r, c=col_tuples, s=sarea, edgecolor='#000000', linewidth=0.2, alpha=1)

    # Add dummy scatter plot to push outer boundary circle away from points plotted above
    _ = ax1.scatter(theta, r + 0.35, c='#000000', s=0, alpha=1)

    # Orbital labels
    rotate_labels = True in (len(lbl) > 2 for lbl in labels)
    # rotate labels by theta[i] if they are longer than 2 characters
    for i in range(norb):
        if rotate_labels:
            rot = 180 * theta[i] / pi
            if rot > 90:
                rot -= 180
            elif rot < -90:
                rot += 180
        else:
            rot = 0

        ax1.text(
            theta[i], r[i] + 0.2, labels[i],
            rotation=rot, size=textsize, ha='center', va='center',
            color=label_colours[i]
        )

    # Add colorbar
    if unit is None:
        cbar_label = 'Individual orbital contribution'
    else:
        cbar_label = 'Individual orbital contribution ({})'.format(unit)
    _ = Colorbar(
        ax2, mapper,
        orientation='vertical',
        label=cbar_label
    )

    # Add the legend to the plot
    ax1.legend(
        legendlines.values(),
        [l.get_label() for l in legendlines.values()],
        bbox_to_anchor=(0.1, 1.0)
    )

    gs.tight_layout(fig, w_pad=5, rect=(0.04, 0.01, 0.98, 0.99))

    if save:
        plt.savefig(fname + '.' + fformat, dpi=300, format=fformat)
        plt.close()
    else:
        plt.show()


def orbdec_analysis(e_mat, min_thr=None, num_thr=4, select_orbs=False, labels=None, label_colours=None,
                    unit=None, save=True, fformat='svg', fname=None):
    norb = e_mat.shape[0]

    # Determine maximum and minimum order of magnitude for off-diagonal elements
    mask = np.ones(e_mat.shape, dtype=bool)
    np.fill_diagonal(mask, 0)
    e_max = e_mat[mask].max()

    if e_max:
        maxthr_power = floor(np.log10(e_max))
    else:
        maxthr_power = -16  # if maximum is 0, set threshold to numerical precision i.e. 1e-16

    # num_thr takes priority over min_thr
    if min_thr is not None and (min_thr >= 10 ** maxthr_power or min_thr < 10 ** (maxthr_power - num_thr + 1)):
        min_thr = None

    if min_thr is None and maxthr_power == -16:
        minthr_power = -16  # don't plot orders of magnitude below numerical precision
    elif min_thr is None:
        minthr_power = maxthr_power - num_thr + 1  # plot num_thr orders of magnitude, 4 by default
    else:
        minthr_power = floor(np.log10(min_thr))

    if labels is None:  # labels for the full orbital set
        labels = [str(i + 1) for i in range(norb)]

    if label_colours is None:
        label_colours = ['#000000'] * norb

    e_diag = np.diagonal(e_mat)
    # 1-orbital circle colours determined by the sum of all elements in the corresponding row
    # The rowsum for orbital i = diagonal element + 1/2 * sum of off-diagonal elements involving i
    # i.e. rowsum from orginal matrix, before the lower triangle was folded onto the upper triangle
    # e_diag = 0.5 * (np.sum(e_mat, axis=0) + np.sum(e_mat, axis=1))
    
    # Select orbitals for plotting
    if not select_orbs and norb > 60:
        select_orbs = True   # for more than 60 orbitals, select by threshold

    nsel = norb
    selected_idx = [i for i in range(norb)]
    orb_labels, orblabel_colours = labels, label_colours

    if select_orbs:
        minthr_power -= 1
        while select_orbs:
            minthr_power += 1  # raise min threshold with every iteration
            selected_idx = [i for i in selected_idx if (e_mat[i] >= 10 ** minthr_power).any()
                            or (e_mat[:, i] >= 10 ** minthr_power).any()]
            nsel = len(selected_idx)
            if nsel <= 60:
                select_orbs = False

        # RE-DEFINING e_mat and e_diag!!!  keeping only the selected orbitals
        e_mat = e_mat[selected_idx, :]
        e_mat = e_mat[:, selected_idx]
        e_diag = e_diag[selected_idx]

        orb_labels = [labels[i] for i in selected_idx]
        orblabel_colours = [label_colours[i] for i in selected_idx]

    # Organise off-diagonal e_mat elements by order of magnitude
    e_dict = {}
    for l in range(maxthr_power - minthr_power + 1):
        e_dict[maxthr_power - l] = []

    # Array of transparency values for all off-diagonal e_mat elements
    alpha_dict = {}  # dict that will contain transparency values for all plotted pairs of orbitals
    # Split transparency scale among orders of magnitude
    alpha_slice = 1.0 / (maxthr_power - minthr_power + 1)
    for i in range(nsel):
        for j in range(i + 1, nsel):
            eij = e_mat[i, j]
            if eij >= 10 ** maxthr_power:
                e_dict[maxthr_power].append((i, j))
                if eij > 10 ** (maxthr_power + 1):
                    alpha_dict[(i, j)] = 1
                else:
                    alpha_dict[(i, j)] = (1 - alpha_slice) + alpha_slice * eij * 10 ** (- maxthr_power - 1)
            else:
                for l in range(maxthr_power - minthr_power):
                    thr_power = maxthr_power - l - 1
                    if 10 ** thr_power <= eij < 10 ** (thr_power + 1):
                        e_dict[thr_power].append((i, j))
                        alpha_dict[(i, j)] = (1 - (l + 2) * alpha_slice) + \
                                             alpha_slice * eij * 10 ** (- thr_power - 1)

    # Plot orbital decomposition
    orbdec_diagram(
        nsel, maxthr_power, e_dict, alpha_dict, e_diag,
        unit=unit, labels=orb_labels, label_colours=orblabel_colours,
        save=save, fname=fname, fformat=fformat
    )


def spindens_orbdec(fockobj, roots=None, min_thr=None):
    if roots is None:
        roots = [0]

    for rt in roots:
        e = np.abs(fockobj.spindms[rt])
        e[e < 1e-16] = 0  # remove numerical noise
        e /= np.trace(e)  # normalise orbdec matrix by its trace

        # Entanglement analysis and diagram
        orbdec_analysis(
            e,
            min_thr=min_thr, save=True, fformat='svg',
            fname='orbdec-spindens_root' + str(rt)
        )
