#!/usr/bin/env python3
import numpy as np


def compute_mo_contributions(mo_vecs, ao_overlap):
    """
    Computes AO percentage contributions to each MO. Orthogonalizes MO
    coefficient vectors using ao_overlap.

    Positional Arguments:
        mo_vecs (np.ndarray)    :  2D array of MO coefficients as column
                                   vectors
        ao_overlap (np.ndarray) :  2D overlap matrix of AO basis functions
        nbas (int)              :  basis size

    Returns:
        mo_percentages (np.ndarray) :  2D ndarray of AO percentage
                                       contributions
    """
    nbas = mo_vecs.shape[0]

    # Diagonalise overlap matrix S and compute S^1/2
    ovlp_evals, ovlp_evecs = np.linalg.eigh(ao_overlap)
    ovlp_sqrt = np.zeros((nbas, nbas), dtype=np.float64)
    np.fill_diagonal(ovlp_sqrt, np.sqrt(ovlp_evals))
    # Convert ovlp_sqrt from its eigenbasis to AO basis
    ovlp_sqrt = ovlp_evecs @ ovlp_sqrt @ ovlp_evecs.T

    # Orthogonalise MO coefficients as S^1/2 C
    mo_vecs = ovlp_sqrt @ mo_vecs
    # resulting matrix is orthonormal (coeffs were already normalised)
    # Create array of percentage contributions
    mo_percentages = 100 * mo_vecs ** 2

    return mo_percentages


def create_basis_labels(bas_id, atom_labels, pattern='cnlm'):
    """
    Creates string labels for each AO group obeying the given pattern.
    Called from group_and_sort.

    Positional arguments:
        bas_id (np.ndarray)        :  array of unique basis identifiers;
                                      columns correspond to
                                      atom index (c), shell (n),
                                      angular momentum (l) and angular momentum
                                      projection (m)
        atom_labels (np.ndarray)   :  array of atom labels (strings)

    Keyword arguments:
        pattern (str)              :  pattern of basis identifiers
                                     ('c', 'cn', 'cl', 'cnl', 'clm' or 'cnlm')

    Returns:
        group_labels (list)        : string labels for the AO groups defined
                                     according to pattern
        bas_ptr (dict)             : dictionary mapping basis group indices to
                                     basis function indices
    """
    nbas = bas_id.shape[0]
    # Make atom index start at 0
    bas_id[:, 0] -= 1
    # Correct shell indices by adding angmom to shell column
    bas_id[:, 1] += bas_id[:, 2]

    if pattern == 'cnlm':
        bas_groups, ngroups, bas_ptr = bas_id, nbas, None
    else:
        # Determine what columns from bas_id to consider
        bas_id_toc = {'c': 0, 'n': 1, 'l': 2, 'm': 3}
        bas_id_cols = []
        for c in list(pattern):
            bas_id_cols.append(bas_id_toc[c])

        # Group basis functions according to pattern
        bas_groups, bas_inv = np.unique(
            bas_id[:, bas_id_cols], return_inverse=True, axis=0
        )
        ngroups = bas_groups.shape[0]

        # Convert bas_inv to dictionary mapping bas_groups rows to bas_id rows
        bas_ptr = {}
        for i in range(nbas):
            if bas_inv[i] in bas_ptr.keys():
                bas_ptr[bas_inv[i]].append(i)
            else:
                bas_ptr[bas_inv[i]] = [i]

    shells_by_angmom = ['s', 'p', 'd', 'f', 'g', 'h', 'i', 'k', 'l']
    # Molcas order for p shell components
    cartcomps = {1: 'x', -1: 'y', 0: 'z'}
    group_labels = []
    for i in range(ngroups):
        lbl = ''
        for j in range(len(pattern)):
            idx = bas_groups[i, j]
            if pattern[j] == 'c':
                lbl += '{:5}'.format(atom_labels[idx])
            elif pattern[j] == 'n':
                lbl += '{:3d}'.format(idx)
            elif pattern[j] == 'l':
                lbl += '{:1}'.format(shells_by_angmom[idx])
            elif pattern[j] == 'm' and bas_groups[i, j - 1] == 1:
                # p sub-shells, print Cartesian label if m is in the pattern
                lbl += '{:1}'.format(cartcomps[idx])
            elif pattern[j] == 'm' and bas_groups[i, j - 1]:
                # all sub-shells except s and p, print m_l quantum number
                lbl += '{:+1d}'.format(idx)
        group_labels.append(lbl)

    return group_labels, bas_ptr


def group_and_sort(mo_percentages, bas_id, atom_labels, pattern='cnlm'):
    """
    Group AO percentage contributions based on the pattern given.

    Positional arguments:
        mo_percentages (np.ndarray) :  2D array of AO percentage contributions
        bas_id (np.ndarray)         :  array of unique basis identifiers
                                       c, n, l, m for each basis function
        nbas (int)                  :  basis size
        atom_labels (np.ndarray)    :  array of unique atom labels stored as
                                       strings (e.g. 'Dy1')

    Keyword arguments:
        pattern (str)               :  string pattern of basis identifiers
                                      ('c', 'cn', 'cl', 'cnl', 'clm' or 'cnlm')
                                       used to group AO contributions

    Returns:
        sorted_percentages (np.ndarray) :  (ngroups, nbas) array of sorted AO
                                            percentage contributions
        sorted_idx (np.ndarray)         :  (ngroups, nbas) array mapping
                                            indices of sorted contributions
                                            to the indices of group_labels
        group_labels (list)             :  list of AO / AO group labels in
                                            normal order
    """
    group_labels, bas_ptr = create_basis_labels(
        bas_id,
        atom_labels,
        pattern=pattern
    )
    ngroups = len(group_labels)
    nbas = bas_id.shape[0]

    if pattern == 'cnlm':
        grouped_percentages = mo_percentages
    else:
        grouped_percentages = np.zeros((ngroups, nbas))
        for i in range(ngroups):
            grouped_percentages[i, :] = np.sum(
                mo_percentages[bas_ptr[i], :], axis=0
            )

    # Sort contributions
    sorted_idx = np.argsort(grouped_percentages, axis=0)
    # Reverse the order of sorted contributions
    sorted_idx = sorted_idx[:: -1, :]
    sorted_percentages = np.take_along_axis(
        grouped_percentages, sorted_idx, axis=0
    )

    return sorted_percentages, sorted_idx, group_labels
