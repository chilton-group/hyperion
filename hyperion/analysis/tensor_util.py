#!/usr/bin/env python3
import math
import numpy as np


def rotation_to_euler_angles(rotmat):
    """
    Reads in a rotation matrix containing COLUMN EIGENVECTORS and returns Euler vectors compatible with EasySpin
    (see https://easyspin.org/easyspin/documentation/eulerangles.html )
    NOTE: EasySpin defines rotation matrices using row eigenvectors,
    i.e. EasySpin rotation matrix = transpose of Hyperion rotation matrix
    To get the correct (EasySpin-compatible) angles, Hyperion uses the transposed version of the matrix provided in
    the link above.

    Input:
        rotmat  (np.ndarray) : 2D rotation matrix where each column is one axis of the final frame,
                               i.e. eigenvector array as resulting from np.linalg.eigh
    Returns:
        eul_ang (np.ndarray) : 1D array containing alpha, beta and gamma Euler angles in radians
    """
    alpha, beta, gamma = 0.0, 0.0, 0.0

    cos_beta = rotmat[2, 2]
    sbcg, sbsg = - rotmat[2, 0], rotmat[2, 1]
    casb, sasb = rotmat[0, 2], rotmat[1, 2]
    sin_beta = math.sqrt(casb ** 2 + sasb ** 2)

    if sin_beta >= 1e-6:  # non-zero sin_beta
        alpha = math.atan2(sasb / sin_beta, casb / sin_beta)
        beta = math.atan2(sin_beta, cos_beta)
        gamma = math.atan2(sbsg / sin_beta, sbcg / sin_beta)
    elif abs(cos_beta - 1) < 1e-6:  # cos(beta) is 1
        alpha = math.atan2(- rotmat[0, 1], rotmat[1, 1])
        beta = 0
        gamma = 0
    elif abs(cos_beta + 1) < 1e-6:  # cos(beta) is -1
        alpha = - math.atan2(rotmat[0, 1], rotmat[1, 1])
        beta = math.pi
        gamma = 0

    eul_ang = np.array([alpha, beta, gamma])
    return eul_ang


def euler_angles_to_rotation(alpha, beta, gamma):
    """
    Constructs rotation matrix containing column eigenvectors of final coordinate system from ZYZ Euler angles.
    """
    sa, ca = math.sin(alpha), math.cos(alpha)
    sb, cb = math.sin(beta), math.cos(beta)
    sg, cg = math.sin(gamma), math.cos(gamma)

    rot_alpha = np.array([[ca, - sa, 0],
                          [sa, ca, 0],
                          [0, 0, 1]])

    rot_beta = np.array([[cb, 0, sb],
                         [0, 1, 0],
                         [- sb, 0, cb]])

    rot_gamma = np.array([[cg, - sg, 0],
                          [sg, cg, 0],
                          [0, 0, 1]])

    return rot_alpha @ rot_beta @ rot_gamma


def reorder_by_overlap(axes1, axes2):
    """
    Reorders column vectors in ndarray axes1 to maximise overlap with column vectors in axes2.
    Input:
        axes1     (np.ndarray) :  (m, n) array of column vectors to be reordered
        axes2     (np.ndarray) :  (m, n) array of column vectors
    Returns:
        new_order (np.ndarray) :  (n,) array containing reordered indices
    """
    ndims = axes1.shape[1]
    new_order = np.zeros(ndims, dtype=np.int32)
    ovlp = np.abs(axes1.T @ axes2)  # not super-sure about taking the absolute value

    # Find max overlap and define first pair, then fill the corresponding row and column with -1 and repeat
    for i in range(ndims):
        best_pair = np.unravel_index(ovlp.argmax(), ovlp.shape)  # 1st index is axes1, 2nd index is axes2
        # permutation of axes1 column indices (new_order values) to match axes2 column indices (new_order indices)
        new_order[best_pair[1]] = best_pair[0]
        ovlp[best_pair[0], :] = - np.ones(ndims)
        ovlp[:, best_pair[1]] = - np.ones(ndims)

    return new_order


def relative_sign_by_magnitude(vals):
    """
    Determines signs of elements in last 2 arrays in vals relative to corresponding elements in vals[0],
    assuming the first 2 arrays in the list (approximately) combine to form the third.
    Signs are stored as 1-dimensional ndarrays of +/-1.
    """
    for i in range(len(vals)):
        if (vals[i] < 0).any():
            vals[i] = np.abs(vals[i])

    nvals = vals[0].shape[0]

    sum_vals = np.abs(vals[0] + vals[1] - vals[2])
    diff_vals = np.abs(np.abs(vals[0] - vals[1]) - vals[2])

    relsign2 = np.zeros(nvals, dtype=np.int8)
    relsign3 = np.zeros(nvals, dtype=np.int8)
    for i in range(nvals):
        if sum_vals[i] <= diff_vals[i]:
            # sum is closer to total, so all values have the same sign
            relsign2[i] = 1
            relsign3[i] = 1
        elif vals[0][i] >= vals[1][i]:
            # difference is closer to total, so vals[0][i] and vals[1][i] have opposite signs
            # if vals[0][i] is larger in magnitude, total inherits its sign
            relsign2[i] = -1
            relsign3[i] = 1
        else:
            relsign2[i] = -1
            relsign3[i] = -1

    return relsign2, relsign3


def two_step_rotation(tens, vecs1, vecs2):
    """
    Take tens, apply inverse rotation using vecs1, followed by a rotation using vecs2
    i.e. vecs2^T . (vecs1 . tens . vecs1^T) . vecs2
    """
    backrot_tens = vecs1 @ tens @ vecs1.T
    return vecs2.T @ backrot_tens @ vecs2
