import numpy as np
import opt_einsum as oe
import os


import hyperion.analysis.orbital_decomposition as orbdec
import hyperion.analysis.orbital_util as orb
from hyperion.electronic_structure.read_h5 import get_rasorb
from hyperion.tools.matrix_tools import matrix_to_file
from hyperion.tools.output_util import write_hfc_orbdec, wrap_line


class HFCOrbDec:
    def __init__(self, wfn, orb_labels=None, guessmo_labels=None, orblabel_fmt=None, orbdec_roots=None,
                 orbdec_min=None, orbdec_nlevels=None, guess_file=None, noplot=False):
        """Produces a 2D matrix for HFC (spin-dependent part) orbital decomposition in the basis of AOs ('uhf' mode),
        active orbitals ('rasscf' mode) or guess MOs ('guess' mode). Only implemented for uhf_mode and rasscf_mode.
        Input:
            TODO - update this
            roots : roots for which to produce orbital decomposition matrices
            guessorb: ndarray of MO row vectors for guess mode"""
        self.rasscf_mode, self.uhf_mode = False, False
        # Determine mode from type of wavefunction object
        if type(wfn).__name__ == 'FockSpace':
            self.rasscf_mode = True
        elif type(wfn).__name__ == 'UHF':
            self.uhf_mode = True

        if self.rasscf_mode:
            self.norb = wfn.nact
            self.ninact = wfn.ninact
            self.mo_types = wfn.mo_types
        else:
            self.norb = wfn.aobasis.nbas
            self.ninact, self.mo_types = None, None

        # Set orbdec options from keyword arguments
        self.noplot = noplot

        allowed_orbfmt = ['c', 'n', 'l', 'cn', 'cl', 'nl', 'lm', 'cnl', 'clm', 'nlm', 'cnlm']
        if orblabel_fmt is not None and orblabel_fmt not in allowed_orbfmt:
            orblabel_fmt = None

        # make roots 0-indexed
        if self.rasscf_mode and orbdec_roots is None:
            self.roots = [0]
        elif self.rasscf_mode and type(orbdec_roots) is not list:
            self.roots = [orbdec_roots - 1]
        elif self.rasscf_mode:
            self.roots = [rt - 1 for rt in orbdec_roots]
        else:
            self.roots = None

        # Set minimum threshold for showing matrix elements on the orbdec diagram
        if orbdec_min is None:
            self.min_thr = None
        else:
            self.min_thr = orbdec_min

        if orbdec_nlevels is None:
            self.num_thr = None
        else:
            self.num_thr = orbdec_nlevels

        if guess_file is None:
            self.do_guess = False
            self.guess_file, self.guessorb, self.ao_ovlp = None, None, None
        else:
            self.do_guess = True
            self.guess_file = guess_file
            self.guessorb = get_rasorb(self.guess_file)
            self.ao_ovlp = wfn.aobasis.ao_ovlp

        # Main orbital labels
        if orb_labels is None:
            if orblabel_fmt is not None and self.rasscf_mode:
                _, sorted_idx, group_lbls = wfn.get_aogroup_percentages(
                    pattern=orblabel_fmt
                )
                all_lbls = [group_lbls[sorted_idx[0, i]] for i in range(sorted_idx.shape[1])]

                self.orb_labels = all_lbls[self.ninact: self.ninact + self.norb]
            elif orblabel_fmt is not None:
                grp_lbls, bas_ptr = orb.create_basis_labels(
                    wfn.aobasis.bas_ids,
                    wfn.aobasis.at_lbls,
                    pattern=orblabel_fmt
                )

                orb_labels = np.empty((wfn.aobasis.nbas, ), dtype=object)
                for grp_id in bas_ptr.keys():
                    orb_labels[bas_ptr[grp_id]] = grp_lbls[grp_id]
                self.orb_labels = orb_labels.tolist()
            elif self.uhf_mode:
                self.orb_labels = wfn.aobasis.bas_labels
            else:
                self.orb_labels = [str(i + 1) for i in range(self.norb)]
        else:
            self.orb_labels = orb_labels

        # Guess MO labels
        if self.do_guess and guessmo_labels is not None:
            self.guessmo_labels = guessmo_labels
        elif self.do_guess and orblabel_fmt is not None:
            _, sorted_idx, group_lbls = orb.group_and_sort(
                orb.compute_mo_contributions(
                    self.guessorb.T,
                    self.ao_ovlp
                ),
                wfn.aobasis.bas_ids,
                wfn.aobasis.at_lbls,
                pattern=orblabel_fmt
            )

            self.guessmo_labels = [group_lbls[sorted_idx[0, i]] for i in range(sorted_idx.shape[1])]
        elif self.do_guess:
            self.guessmo_labels = [str(i + 1) for i in range(self.norb)]

        # Write out theory to standard output
        write_hfc_orbdec(uhf_ao=self.uhf_mode, uhf_guess=self.do_guess)

        # List of all files (name+extension) created during the current Hyperion run
        self.file_list = []

        # Holds current orbital decomposition matrix
        self.orbdec_mat = None

    def make_orbdec_matrix(self, fcsd_orb, spindm):
        # Build spatial part of orbdec matrix (vector norm of h operator)
        # factor of 1/S already included in fcsd_orb when analysis methods are called from HyperfineCoupling
        fcsd_z = np.zeros((self.norb, self.norb))
        for i in range(self.norb):
            for j in range(i, self.norb):  # only the upper triangle is computed
                magn_z = fcsd_orb[0, i, j] ** 2 + fcsd_orb[1, i, j] ** 2 + fcsd_orb[2, i, j] ** 2
                if magn_z > 1e-30:
                    fcsd_z[i, j] = np.sqrt(magn_z)
                if j != i:
                    fcsd_z[i, j] *= 2  # fold lower triangle over upper triangle, work only with the latter

        self.orbdec_mat = np.abs(0.5 * spindm * fcsd_z)
        self.orbdec_mat[self.orbdec_mat < 1e-16] = 0  # remove numerical noise

    def analysis_uhf(self, fcsd_orb, spindm, nuc_id):
        self.make_orbdec_matrix(fcsd_orb, spindm)

        fname = 'orbdec-fcsd_AO_{}'.format(nuc_id)
        matrix_to_file(self.orbdec_mat, filename='{}.dat'.format(fname))
        self.file_list.append('{}.dat'.format(fname))

        if not self.noplot:
            orbdec.orbdec_analysis(
                self.orbdec_mat,
                min_thr=self.min_thr,
                num_thr=self.num_thr,
                select_orbs=True,
                unit='MHz',
                labels=self.orb_labels,
                save=True,
                fformat='svg',
                fname=fname
            )

            self.file_list.append('{}.svg'.format(fname))

        if self.do_guess:
            # Transform spin DM and fcsd_orb to guess MO basis if in guess mode
            spindm = oe.contract('ai,ik,kl,lj,bj->ab', self.guessorb, self.ao_ovlp, spindm,
                                 self.ao_ovlp, self.guessorb)
            fcsd_orb = oe.contract('ai,kij,bj->kab', self.guessorb, fcsd_orb, self.guessorb)

            self.make_orbdec_matrix(fcsd_orb, spindm)

            fname = 'orbdec-fcsd_MO_{}'.format(nuc_id)
            matrix_to_file(self.orbdec_mat, filename='{}.dat'.format(fname))
            self.file_list.append('{}.dat'.format(fname))

            if not self.noplot:
                orbdec.orbdec_analysis(
                    self.orbdec_mat,
                    min_thr=self.min_thr,
                    num_thr=self.num_thr,
                    select_orbs=True,
                    unit='MHz',
                    labels=self.guessmo_labels,
                    save=True,
                    fformat='svg',
                    fname=fname
                )

                self.file_list.append('{}.svg'.format(fname))

    def analysis_rasscf(self, fcsd_orb, spindm, nuc_id):
        # Colour code RAS2 orbital labels as orange, keep the rest black
        orblabel_colours = ['#d77005' if t == '2' else '#000000' for t in self.mo_types]
        orblabel_colours = orblabel_colours[self.ninact: self.ninact + self.norb]

        for rt in self.roots:
            self.make_orbdec_matrix(fcsd_orb, spindm[rt])

            fname = 'orbdec-fcsd_{}_root{}'.format(nuc_id, rt + 1)
            matrix_to_file(self.orbdec_mat, filename='{}.dat'.format(fname))
            self.file_list.append('{}.dat'.format(fname))

            if not self.noplot:
                orbdec.orbdec_analysis(
                    self.orbdec_mat,
                    min_thr=self.min_thr,
                    num_thr=self.num_thr,
                    labels=self.orb_labels,
                    label_colours=orblabel_colours,
                    unit='MHz',
                    save=True,
                    fformat='svg',
                    fname=fname
                )

                self.file_list.append('{}.svg'.format(fname))

    def write_filelist(self):
        filelist_str = '** files: {}'.format(' '.join(self.file_list))
        wrap_line(filelist_str)
        print('    have been saved to directory {}\n'.format(os.getcwd()))
        print()
