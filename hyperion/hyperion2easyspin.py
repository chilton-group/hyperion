#!/usr/bin/env python3
import argparse
import numpy as np

import pandas as pd
import re
import shutil

import hyperion.tools.dataframe_util as dfutil
from hyperion.analysis.tensor_util import euler_angles_to_rotation, two_step_rotation
from hyperion.tools.output_util import write_utility_header


def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('csvfile', help='CSV file containing EPR parameters from Hyperion')

    parser.add_argument('--mult_id', default=None, type=int,
                        help='multiplet ID used to select the parameters to be exported to EasySpin; '
                             'if not specified, the lowest multiplet id available is chosen')

    parser.add_argument('--seff', default=None, type=float,
                        help='effective spin used to select the parameters to be exported to EasySpin; '
                             'can be used instead of mult_id as selection criterion, however note that '
                             'this keyword does not discriminate between different multiplet IDs')

    parser.add_argument('--isotopes', default=None, nargs='*', type=str,
                        help='one or more isotope labels (e.g. 13C) used as selection criteria')

    parser.add_argument('--atom_ids', default=None, nargs='*', type=str,
                        help='one or more atom IDs (e.g. C1) used as selection criteria; note that, unlike '
                             'the main Hyperion input, mass numbers should NOT be included in the atom ID')

    parser.add_argument('--out', default=None, type=str,
                        help='name of EasySpin (Matlab) script file, with or without the .m extension; '
                             'if not provided, the name of the input .csv file is used')

    parser.add_argument('--prologue', default=None, type=str,
                        help='file containing EasySpin (Matlab) code to be inserted BEFORE the generated input')

    parser.add_argument('--epilogue', default=None, type=str,
                        help='file containing EasySpin (Matlab) code to be inserted AFTER the generated input')

    args = parser.parse_args()

    return args


def process_dataframe(fname, mult_id=None, seff=None, isotopes=None, atom_ids=None):
    used_cols = [
        'property',
        'multiplet_id',
        'effective_spin',
        'isotope',
        'atom_id',
        'total_eigval1',
        'total_eigval2',
        'total_eigval3',
        'total_euler_alpha',
        'total_euler_beta',
        'total_euler_gamma'
    ]

    df = pd.read_csv(fname, usecols=used_cols, dtype=dfutil.export_column_dtypes)

    # Select rows corresponding to a single multiplet and/or other provided criteria
    mask = True
    # mask = (df.property == 'Zeeman') | (df.property == 'HyperfineCoupling')
    if mult_id is None:
        mult_values = df.multiplet_id.unique()
        if mult_values.size > 1:
            mult_id = mult_values.min  # choose the multiplet with the lowest index
            mask = (df.multiplet_id == mult_id)
    else:
        mask = (df.multiplet_id == mult_id)

    if seff is not None:
        mask = mask & (df.effective_spin == seff)

    if isotopes is not None:
        mask = mask & ((df.isotope.isin(isotopes)) | (df.property == 'Zeeman'))

    if atom_ids is not None:
        mask = mask & ((df.atom_id.isin(atom_ids)) | (df.property == 'Zeeman'))

    if type(mask) is not bool:
        df = df.loc[mask]

    return df


def gen_easyspin_input(fobj, df):
    # Find g-tensor first and initialise the Sys variable
    rows = df.loc[df.property == 'Zeeman'].index.to_numpy()
    idx = rows[0]  # only one g-tensor per file (for now)

    rotation_gaxes = euler_angles_to_rotation(df.loc[idx, 'total_euler_alpha'],
                                              df.loc[idx, 'total_euler_beta'],
                                              df.loc[idx, 'total_euler_gamma'])

    line = 'Sys.S = {:.1f};\n'.format(df.loc[idx, 'effective_spin'])
    fobj.write(line)

    line = 'Sys.g = [{:.2f} {:.2f} {:.2f}];\n'.format(
        df.loc[idx, 'total_eigval1'],
        df.loc[idx, 'total_eigval2'],
        df.loc[idx, 'total_eigval3']
    )
    fobj.write(line)

    # Find and write HFC parameters
    rows = df.loc[df.property == 'HyperfineCoupling'].index.to_numpy()
    iso_list = []
    hfcstr_list = []

    for idx in rows:
        atens = np.zeros((3, 3), dtype=np.float)
        np.fill_diagonal(atens, [df.loc[idx, 'total_eigval1'],
                                 df.loc[idx, 'total_eigval2'],
                                 df.loc[idx, 'total_eigval3']])

        atens = two_step_rotation(atens,
                                  euler_angles_to_rotation(df.loc[idx, 'total_euler_alpha'],
                                                           df.loc[idx, 'total_euler_beta'],
                                                           df.loc[idx, 'total_euler_gamma']),
                                  rotation_gaxes)

        iso_list.append(df.loc[idx, 'isotope'])
        hfcstr_list.append(
            '[{:.3f} {:.3f} {:.3f}; {:.3f} {:.3f} {:.3f}; {:.3f} {:.3f} {:.3f}]'.format(
                   *atens.flatten().tolist()
               )
        )

    line = 'Sys.Nucs = \'{}\';\n'.format(','.join(iso_list))
    fobj.write(line)

    line = 'Sys.A = [{}];\n'.format('; '.join(hfcstr_list))
    fobj.write(line)


def hyperion2easyspin():
    args = parse_arguments()

    df = process_dataframe(args.csvfile,
                           mult_id=args.mult_id,
                           seff=args.seff,
                           isotopes=args.isotopes,
                           atom_ids=args.atom_ids)

    if args.out is None:
        m = re.match('(\w+)\.csv', args.csvfile)
        script_file = '{}.m'.format(m.group(1))
    else:
        m = re.match('(\w+)\.m', args.out)
        if m is None:
            script_file = '{}.m'.format(args.out)
        else:
            script_file = args.out

    f = open(script_file, 'w+')
    write_utility_header('hyperion2easyspin', fobj=f, comment_char='% ')

    f.write('% Input options:\n')
    f.write('% csv file: {}\n'.format(args.csvfile))
    if args.mult_id is not None:
        f.write('% multiplet ID: {:d}\n'.format(args.mult_id))
    if args.seff is not None:
        f.write('% effective spin: {:.1f}\n'.format(args.seff))
    if args.isotopes is not None:
        f.write('% isotopes: {}\n'.format(' '.join(args.isotopes)))
    if args.atom_ids is not None:
        f.write('% nuclei: {}\n'.format(' '.join(args.atom_ids)))
    if args.out is not None:
        f.write('% output: {}\n'.format(args.out))
    if args.prologue is not None:
        f.write('% prologue: {}\n'.format(args.prologue))
    if args.epilogue is not None:
        f.write('% epilogue: {}\n'.format(args.epilogue))
    f.write('\n')

    if args.prologue is not None:
        with open(args.prologue, 'r') as fsrc:
            shutil.copyfileobj(fsrc, f)
        f.write('\n')

    # The main event: generate EasySpin input from the data in the csv file
    f.write('%' + 74 * '=' + '\n')
    f.write('% Parameters computed by Hyperion\n')
    f.write('%' + 74 * '=' + '\n\n')
    gen_easyspin_input(f, df)
    f.write('\n%' + 74 * '=' + '\n')

    if args.epilogue is not None:
        with open(args.epilogue, 'r') as fsrc:
            shutil.copyfileobj(fsrc, f)
        f.write('\n')

    f.close()
