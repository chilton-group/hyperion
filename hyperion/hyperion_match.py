#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd
import re

import hyperion.tools.dataframe_util as dfutil
from hyperion.analysis.tensor_util import euler_angles_to_rotation, two_step_rotation
from hyperion.tools.output_util import write_utility_header, write_3by3_tensor


def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('file1', help='CSV file containing spin-only parameters')
    parser.add_argument('file2', help='CSV file containing pseudospin parameters')

    parser.add_argument('--pair', default=None, nargs=2, type=int,
                        help='pair of multiplet indices to match; the first index corresponds to the spin-only'
                             'dataset (first CSV file), the second index corresponds to the pseudospin dataset'
                             '(second file)')
    parser.add_argument('--pairs', default=None,
                        help='plain text file containing pairs of multiplet indices to match;'
                             'the file should contain one pair per row, with space-separated indices')
    parser.add_argument('--dryrun', action='store_true', default=False,
                        help='does not modify the pseudospin file; only prints transformed spin-only'
                             'tensors and inferred signs to standard output')

    args = parser.parse_args()

    return args


def read_pairs_file(filename):
    pairs = np.genfromtxt(filename, dtype=np.int)

    assert (len(pairs.shape) == 2 & pairs.shape[1] == 2), "The pairs file has an invalid format!"

    return pairs


def transform_and_infer_signs(hfcc_spinonly, axes_spinonly, hfcc_pseudospin, axes_pseudospin):
    a_spinonly = np.zeros((3, 3), dtype=np.float64)
    np.fill_diagonal(a_spinonly, hfcc_spinonly)
    a_spinonly = two_step_rotation(a_spinonly, axes_spinonly, axes_pseudospin)

    diag_signs = np.sign(a_spinonly.diagonal()).astype(int)
    diag_signs[diag_signs == 0] = 1

    print('''  Spin-only tensor (a_spin) in the eigenframe (u1, u2, u3)
  of the spin part of the pseudospin tensor (a_pseudoS_FCSD):\n''')
    write_3by3_tensor(a_spinonly, row_labels=['u1', 'u2', 'u3'], column_labels=['u1', 'u2', 'u3'])

    print('  Signs inferred for the pseudospin FC+SD eigenvalues:\n')
    print('  Eigenvalue  |{:10.3f} |{:10.3f} |{:10.3f} |'.format(*hfcc_pseudospin))
    print('  ' + 49 * '-')
    print('  Sign        |{:>+10d} |{:>+10d} |{:>+10d} |\n'.format(*diag_signs))

    hfcc_pseudospin_signed = diag_signs * hfcc_pseudospin
    
    diff_tens = np.zeros((3, 3), dtype=np.float64)
    np.fill_diagonal(diff_tens, hfcc_pseudospin_signed)
    diff_tens = diff_tens - a_spinonly
    print('  Trace and determinant of (a_pseudoS_FCSD - a_spin):{:12.3e} {:12.3e}\n'.format(
        np.trace(diff_tens), np.linalg.det(diff_tens)))
    print('''  In the limit of perfect agreement between spin-only and pseudospin parametrisations,
  both the trace and the determinant of this difference are 0.\n''')

    return diag_signs, hfcc_pseudospin_signed


def match_parameters(df1, df2, pairs, dry_run=False):
    for i, j in zip(pairs[:, 0], pairs[:, 1]):
        print('** SPIN-ONLY MULTIPLET {} -- PSEUDOSPIN MULTIPLET {} **\n'.format(i, j))
        mask = (df1.multiplet_id == i) & (df1.property == 'HyperfineCoupling') & (df1.parametrisation == 'spin-only')
        rows1 = df1.loc[mask]
        mask = (df2.multiplet_id == j) & (df2.property == 'HyperfineCoupling') & (df2.parametrisation == 'pseudospin')
        rows2 = df2.loc[mask]

        # Only look at nuclei (i.e. combination of atom ID and isotope) found in both datasets
        mask = (rows1.atom_id.isin(rows2.atom_id)) & (rows1.isotope.isin(rows2.isotope))
        rows1 = rows1.loc[mask]
        idx1_list = rows1.drop_duplicates(subset=['atom_id', 'isotope'], inplace=False).index.to_numpy()

        for idx1 in idx1_list:
            at_idx = rows1.loc[idx1, 'atom_id']
            isotope = rows1.loc[idx1, 'isotope']
            print(' * ATOM {}, isotope {}\n'.format(at_idx, isotope))

            # Select first entry in pseudospin dataframe with matching atom_id and isotope
            mask = (rows2.atom_id == at_idx) & (rows2.isotope == isotope)
            idx2 = rows2.loc[mask].index.to_numpy()[0]

            hfcc_spinonly = rows1.loc[idx1, ['total_eigval1',
                                             'total_eigval2',
                                             'total_eigval3']].to_numpy()
            eulang_spinonly = rows1.loc[idx1, ['total_euler_alpha',
                                               'total_euler_beta',
                                               'total_euler_gamma']].to_numpy()

            hfcc_pseudos = rows2.loc[idx2, ['spin_part_eigval1',
                                            'spin_part_eigval2',
                                            'spin_part_eigval3']].to_numpy()
            eulang_pseudos = rows2.loc[idx2, ['spin_part_euler_alpha',
                                              'spin_part_euler_beta',
                                              'spin_part_euler_gamma']].to_numpy()

            diag_signs, hfcc_pseudos_signed = transform_and_infer_signs(hfcc_spinonly,
                                                                        euler_angles_to_rotation(*eulang_spinonly),
                                                                        hfcc_pseudos,
                                                                        euler_angles_to_rotation(*eulang_pseudos))

            if not dry_run:
                df2.loc[idx2, ['spin_part_eigval1',
                               'spin_part_eigval2',
                               'spin_part_eigval3']] = hfcc_pseudos_signed

                relsign_cols = ['orbital_part_relative_sign1',
                                'orbital_part_relative_sign2',
                                'orbital_part_relative_sign3',
                                'total_relative_sign1',
                                'total_relative_sign2',
                                'total_relative_sign3']
                relsign_present = all([col in df2.columns for col in relsign_cols])
                if relsign_present:
                    orbital_signs = df2.loc[idx2, ['orbital_part_relative_sign1',
                                                   'orbital_part_relative_sign2',
                                                   'orbital_part_relative_sign3']].to_numpy() * diag_signs
                    total_signs = df2.loc[idx2, ['total_relative_sign1',
                                                 'total_relative_sign2',
                                                 'total_relative_sign3']].to_numpy() * diag_signs
                    df2.loc[idx2, ['orbital_part_eigval1',
                                   'orbital_part_eigval2',
                                   'orbital_part_eigval3']] *= orbital_signs
                    df2.loc[idx2, ['total_eigval1',
                                   'total_eigval2',
                                   'total_eigval3']] *= total_signs

    if not dry_run:
        return df2


def hyperion_match():
    args = parse_arguments()

    write_utility_header('hyperion_match')
    print('File 1 (spin-only data)  : {}'.format(args.file1))
    print('File 2 (pseudospin data) : {}\n'.format(args.file2))

    df1 = pd.read_csv(args.file1, dtype=dfutil.export_column_dtypes)
    df2 = pd.read_csv(args.file2, dtype=dfutil.export_column_dtypes.update(dfutil.relsign_column_dtypes))

    if args.pairs is not None:
        pairs = read_pairs_file(args.pairs)
    elif args.pair is not None:
        pairs = np.array(args.pair)
        pairs = pairs[None, :]  # make pairs 2D for consistency
    else:
        # if pairs are not specified by user, match identical multiplet ids
        common_mult = np.intersect1d(df1.multiplet_id.to_numpy(),
                                     df2.multiplet_id.to_numpy())
        pairs = np.tile(common_mult[:, None], (1, 2))

    if args.dryrun:
        dry_run = True
        print('DRY RUN mode: ON (not generating a new file with modified pseudospin eigenvalues)\n')
    else:
        # Count occurrences of pseudospin multiplet IDs, enable dry run if any multiplet occurs more than once
        vals, mult_counts = np.unique(pairs[:, 1], return_counts=True)
        if (mult_counts != 1).any():
            dry_run = True
            print('Many-to-one multiplet mapping detected, enabling DRY RUN mode...')
            print('DRY RUN mode: ON (not generating a new file with modified pseudospin eigenvalues)\n')
        else:
            dry_run = False

    if dry_run:
        match_parameters(df1, df2, pairs, dry_run=True)
    else:
        df2 = match_parameters(df1, df2, pairs, dry_run=False)
        m = re.match('(\w+)\.csv', args.file2)
        new_file = m.group(1) + '_modified.csv'
        df2.to_csv(new_file, index=False)
        print('\nThe updated data, including signed eigenvalues, has been written to {}\n'.format(new_file))
