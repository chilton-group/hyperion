#!/usr/bin/env python3
import os
import configparser


def find_libcint():
    # Initialise libcint_path as None
    libcint_path = None

    # Look for local libcint installation
    depsdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'hyperion/dependencies')

    if os.path.isdir(os.path.join(depsdir, 'lib')) and (os.path.isfile(os.path.join(depsdir, 'lib/libcint.so'))
                                                        | os.path.isfile(os.path.join(depsdir, 'lib/libcint.dylib'))):
        libcint_path = os.path.join(depsdir, 'lib')
    elif os.path.isdir(os.path.join(depsdir, 'lib64')) and (os.path.isfile(os.path.join(depsdir, 'lib64/libcint.so'))
                                                        | os.path.isfile(os.path.join(depsdir, 'lib64/libcint.dylib'))):
        libcint_path = os.path.join(depsdir, 'lib64')
    else:
        # If not found, look in LD_LIBRARY_PATH
        libenv = os.environ['LD_LIBRARY_PATH'].split(':')
        for libdir in libenv:
            if os.path.isfile(os.path.join(libdir, 'libcint.so')) | \
                    os.path.isfile(os.path.join(libdir, 'libcint.dylib')):
                libcint_path = libdir
                break

    return libcint_path


def write_config(libcint_path):
    config = configparser.ConfigParser()

    config.add_section('libcint')
    config.set('libcint', 'path', libcint_path)
    config.set('libcint', 'name', 'libcint')

    cfg_fname = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'hyperion/config.ini')
    with open(cfg_fname, 'w+') as cfgfile:
        config.write(cfgfile)


if __name__ == '__main__':
    libcint_path = find_libcint()

    if libcint_path is None:
        write_config('')
        raise Exception('''libcint.so was not found in ./hyperion/dependencies or in any $LD_LIBRARY_PATH directory. 
Run build_libcint.sh in the ./hyperion/dependencies subdirectory to build a local version
or manually enter the path to an external libcint in ./hyperion/config.ini .''')
    else:
        write_config(libcint_path)
