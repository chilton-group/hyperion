#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import setuptools

# DO NOT EDIT THIS NUMBER!
# IT IS AUTOMATICALLY CHANGED BY python-semantic-release
__version__ = "1.0.1"

setuptools.setup(
    name='hyperion',
    version=__version__,
    author='Letitia Birnoschi',
    author_email='letitia.birnoschi@manchester.ac.uk',
    description='Package for computing relativistic ab initio hyperfine coupling constants',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent'
    ],

    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'opt_einsum',
        'h5py',
        'pandas',
        'matplotlib',
    ],
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'hyperion=hyperion:main',
            'hyperion_match=hyperion.hyperion_match:hyperion_match',
            'hyperion2easyspin=hyperion.hyperion2easyspin:hyperion2easyspin'
        ]
    }
)
